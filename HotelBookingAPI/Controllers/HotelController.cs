﻿// HotelController.cs
using Application.IRepository;
using BusinessObject.DTOs.Hotel;
using BusinessObject.Model;
using Microsoft.AspNetCore.Mvc;

namespace HotelBookingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HotelController : ControllerBase
    {
        private IHotelRepository _hotelRepository;

        public HotelController(IHotelRepository hotelRepository)
        {
            _hotelRepository = hotelRepository;
        }

        // GET: api/hotel
        [HttpGet]
        public async Task<ActionResult<IEnumerable<HotelDTO>>> GetHotelDTOs()
        {
            var hotels = await _hotelRepository.GetAllHotelsAsync();
            var hotelDTOs = MapToHotelDTOs(hotels);
            return Ok(hotelDTOs);
        }

        // GET: api/hotel/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<HotelDTO>> GetHotelDTO(string id)
        {
                var hotel = await _hotelRepository.GetHotelByIdAsync(id);
                if (hotel == null)
                {
                    return NotFound();
                }
            var hotelDTO = MapToHotelDTO(hotel);
            return Ok(hotelDTO);
        }

        // POST: api/hotel
        [HttpPost]
        public async Task<ActionResult<HotelDTO>> CreateHotelDTO(HotelCreateDTO hotelDTO)
        {
            await _hotelRepository.AddHotelAsync(hotelDTO);
            return NoContent();
            
        }

        // PUT: api/hotel/{id}
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateHotelDTO(string id, HotelDTO hotelDTO)
        {
            if (id != hotelDTO.HotelId)
            {
                return BadRequest("Hotel ID mismatch");
            }

            var hotel = MapToHotel(hotelDTO);
            await _hotelRepository.UpdateHotelAsync(hotel);
            return NoContent();

        }

        // DELETE: api/hotel/{id}
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteHotelDTO(string id)
        {
            await _hotelRepository.DeleteHotelAsync(id);
            return NoContent();
        }

        private HotelDTO MapToHotelDTO(Hotel hotel)
        {
            return new HotelDTO
            {
                HotelId = hotel.HotelId,
                Address = hotel.Address,
                Name = hotel.Name,
                Image = hotel.Image,
                AverageRating = hotel.AverageRating,
                Decription = hotel.Decription
            };
        }

        private IEnumerable<HotelDTO> MapToHotelDTOs(IEnumerable<Hotel> hotels)
        {
            return hotels.Select(hotel => MapToHotelDTO(hotel));
        }

        private Hotel MapToHotel(HotelDTO hotelDTO)
        {
            return new Hotel
            {
                HotelId = hotelDTO.HotelId,
                Address = hotelDTO.Address,
                Name = hotelDTO.Name,
                Image = hotelDTO.Image,
                AverageRating = hotelDTO.AverageRating,
                Decription = hotelDTO.Decription
            };
        }

        [HttpGet("hotel-address")]
        public async Task<ActionResult<List<HotelDTO>>> SearchHotelAddress(string address)
        {
            var ad = await _hotelRepository.SearchHotelByAddress(address);
            return Ok(new
            {
                StatusCode = 200,
                Result = true,
                Message = $"Get address successful!",
                Data = ad
            });
        }

        [HttpGet("hotel-name")]
        public async Task<ActionResult<List<HotelDTO>>> SearchHotelByName(string name)
        {
            var hotels = await _hotelRepository.GetHotelByName(name);
            if(hotels == null)
            {
                return NotFound();
            }
            return Ok();
        }
    }
}
