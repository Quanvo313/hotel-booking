﻿using Application.IRepository;
using BusinessObject.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace HotelBookingAPI.Controllers
{
    [Route("api/payments")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly IPaymentRepository _paymentRepository;

        public PaymentController(IPaymentRepository paymentRepository)
        {
            _paymentRepository = paymentRepository;
        }

        [HttpPost("create-payment")]
        public async Task<ActionResult<PaymentDetailDTO>> AddAccount(PaymentCreateDTO paymentCreate)
        {
            var payment = await _paymentRepository.Insert(paymentCreate);
            return Ok(payment);
        }

        [HttpGet]
        public async Task<ActionResult<List<PaymentDTO>>> GetAllPayments()
        {
            var payments = await _paymentRepository.GetAll();
            return Ok(payments);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PaymentDetailDTO>> GetPaymentById(String id)
        {
            var payment = await _paymentRepository.GetByID(id);
            if (payment == null)
            {
                return NotFound(); // Return a 404 Not Found if payment is not found.
            }
            return Ok(payment);
        }

        [HttpGet("date/{date}")]
        public async Task<ActionResult<IEnumerable<PaymentDetailDTO>>> GetPaymentsByDate(DateTime date)
        {
            var payments = await _paymentRepository.GetByDate(date);
            return Ok(payments);
        }

        [HttpGet("dateRange/{from}/{to}")]
        public async Task<ActionResult<IEnumerable<PaymentDetailDTO>>> GetPaymentsByDateRange(DateTime from, DateTime to)
        {
            var payments = await _paymentRepository.GetByDateRange(from, to);
            return Ok(payments);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<bool>> UpdatePayment(String id, PaymentDetailDTO updatedPayment)
        {
            if (id != updatedPayment.PaymentId)
            {
                return BadRequest("ID mismatch"); // Handle ID mismatch error.
            }
            await _paymentRepository.Update(updatedPayment);
            return NoContent(); // Return a 204 No Content response.
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> DeletePayment(string id)
        {
            await _paymentRepository.Delete(id);
            return NoContent(); // Return a 204 No Content response.
        }
    }
}
