﻿using Application.IRepository;
using BusinessObject.DTOs.Booking;
using Microsoft.AspNetCore.Mvc;

namespace HotelBookingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookingController : ControllerBase
    {
        private readonly IBookingRepository _bookingRepository;
        public BookingController(IBookingRepository bookingRepository)
        {
            _bookingRepository = bookingRepository;
        }

        [HttpGet("get-all-booking")]
        public async Task<IActionResult> GetAllBooking()
        {
            var booking = await _bookingRepository.GetAll();
            return Ok(booking);
        }

        [HttpGet("get-booking-by-id/{id}")]
        public async Task<IActionResult> GetBookingById(string id)
        {
            var booking = await _bookingRepository.GetBookingById(id);
            return Ok(booking);
        }

        [HttpPost("create-booking")]
        public async Task<IActionResult> CreateBooking(BookingCreateDTO booking)
        {
            await _bookingRepository.CreateBooking(booking);
            return Ok();
        }

        [HttpPut("update-booking")]
        public async Task<IActionResult> UpdateBooking(BookingDTO booking)
        {
            await _bookingRepository.UpdateBooking(booking);
            return Ok();
        }
    }
}
