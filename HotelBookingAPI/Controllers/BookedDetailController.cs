﻿using Application.IRepository;
using BusinessObject.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace HotelBookingAPI.Controllers
{
    [Route("api/bookeddetail")]
    [ApiController]
    public class BookedDetailController : ControllerBase
    {
        private IBookedDetailRepository _bookedDetailRepository;
        public BookedDetailController(IBookedDetailRepository bookedDetailRepository)
        {
            _bookedDetailRepository = bookedDetailRepository;
        }

        [HttpGet("get-all")]
        public async Task<ActionResult<List<BookedDetailDTO>>> GetBookedDetails()
        {
            var detail = await _bookedDetailRepository.GetListBookedDetails();
            if(!detail.Any())
            {
                return NotFound("Current no detail");
            }
            return Ok(detail);
        }
        [HttpGet("get-id")] 
        public async Task<ActionResult<BookedDetailDTO>> GetById(string id)
        {
            var detail = await _bookedDetailRepository.GetBookedDetailsById(id);
            if (detail != null)
            {
                return Ok(detail);
            }
            return NotFound();
        }

        [HttpPost("add")]
        public async Task<ActionResult<BookedDetailDTO>> Add(BookedDetailDTO bookedDetailDTO)
        {
            BookedDetailDTO detailDTO = await _bookedDetailRepository.CreateBookedDetail(bookedDetailDTO);
            if (detailDTO == null)
            {
                return BadRequest();
            }
            return Ok(detailDTO);
        }

        [HttpPut("update")]
        public async Task<ActionResult<BookedDetailDTO>> Update(BookedDetailDTO bookedDetailDTO)
        {
            if (bookedDetailDTO == null)
            {
                return NotFound();
            }
            var detailDTO = await _bookedDetailRepository.UpdateBookedDetail(bookedDetailDTO);
            return Ok(detailDTO);
        }

        [HttpDelete("delete")]
        public async Task<ActionResult> Delete(string id)
        {
            bool isSuccess = await _bookedDetailRepository.DeleteBookedDetail(id);
            if (!isSuccess) { return BadRequest(); }
            return Ok("Booked detail deleted successfully");
        }
        [HttpDelete("undelete")]
        public async Task<ActionResult> UnDelete(string id)
        {
            await _bookedDetailRepository.UnDeleteBookedDetail(id);
            return Ok("Booked detail undeleted successfully");
        }
    }
}
