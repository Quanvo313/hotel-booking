﻿using Application.IRepository;
using BusinessObject.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace HotelBookingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpGet("get-all-user")]
        public async Task<ActionResult<List<UserDTO>>> GetAllUser()
        {
            var userlist = await _userRepository.GetListUser();
            if (!userlist.Any())
            {
                return NotFound("Current no user");
            }
            return Ok(userlist);
        }
        [HttpGet("get-id-user")]
        public async Task<ActionResult> GetUserById(string id)
        {
            var useid = await _userRepository.GetUserById(id);
            if (useid != null)
            {
                return Ok(useid);
            }
            return NotFound();
        }

        [HttpPost("add-user")]
        public async Task<ActionResult<UserDTO>> Add(UserDTO userDTO)
        {
            UserDTO newUser = await _userRepository.CreateUser(userDTO);
            if (newUser == null)
            {
                return BadRequest($"User '{userDTO.Name}' has been existed");
            }
            return Ok(newUser);
        }

        [HttpPut("update-user")]
        public async Task<ActionResult<UserDTO>> Update(UserDTO userDTO)
        {
            UserDTO _userDTO = await _userRepository.UpdateUser(userDTO);
            if (_userDTO == null)
            {
                return BadRequest($"User '{userDTO.Name}' has not been existed");
            }
            return Ok(_userDTO);
        }

        [HttpDelete("delete-user")]
        public async Task<ActionResult> Delete(string id)
        {
            bool isSuccess = await _userRepository.DeleteUser(id);
            if (!isSuccess) { return BadRequest(); }
            return Ok("User deleted successfully");
        }

        [HttpDelete("undelete-user")]
        public async Task<ActionResult> UnDelete(string id)
        {
            await _userRepository.UnDeleteUser(id);
            return Ok("User undeleted successfully");
        }

        [HttpGet("name/{name}")]
        public async Task<ActionResult<IEnumerable<UserDTO>>> SearchUsersByName(string name)
        {
            var res = await _userRepository.GetUsersByName(name);
            return Ok(res);
        }
        [HttpGet("phone/{phoneNumber}")]
        public async Task<ActionResult<IEnumerable<UserDTO>>> SearchUsersByPhone(string phoneNumber)
        {
            var res = await _userRepository.GetUsersByPhone(phoneNumber);
            return Ok(res);
        }
        [HttpGet("email/{email}")]
        public async Task<ActionResult<IEnumerable<UserDTO>>> SearchUsersByEmail(string email)
        {
            var res = await _userRepository.GetUsersByEmail(email);
            return Ok(res);
        }
    }
}
