﻿using Application.IRepository;
using Microsoft.AspNetCore.Mvc;
using BusinessObject.Model;
using BusinessObject.DTOs;

namespace HotelBookingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomTypeController : ControllerBase
    {
        private readonly IRoomTypeRepository _RoomTypeRepository;

        public RoomTypeController(IRoomTypeRepository RoomTypeRepository)
        {
            _RoomTypeRepository = RoomTypeRepository;
        }

        [HttpGet("get-all")]
        public async Task<ActionResult> doGet()
        {
            var List = await _RoomTypeRepository.getAll();
            return Ok(List);
        }


        [HttpGet("get-by-id")]
        public async Task<ActionResult> doGetId(string id)
        {
            RoomType rt = await _RoomTypeRepository.getRoomTypeId(id);
            return Ok(rt);
        }

        [HttpPost]
        public async Task<ActionResult> doPost(RoomType rt) 
        {
            await _RoomTypeRepository.addRoomType(rt);
            return Ok();
        }

        [HttpPut]
        public async Task<ActionResult> doPut(RoomType _rt)
        {
            var rt = await _RoomTypeRepository.updateRoomType(_rt);
            return Ok(rt);
        }

        [HttpPatch]
        public async Task<ActionResult<RoomType>> doDelete(string nam)
        {
            var rt = await _RoomTypeRepository.removeRoomType(nam);
            return Ok(rt);
        }

        [HttpGet("room-by-roomtype")]
        public async Task<ActionResult<RoomTypeDTO>> GetRoomByRTName(string name)
        {
            var rt = await _RoomTypeRepository.GetRoomByRTName(name);
            return Ok(new
            {
                StatusCode = 200,
                Result = true,
                Message = $"Get room successful!",
                Data = rt
            });
        }
    }
}
