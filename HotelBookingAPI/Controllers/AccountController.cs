﻿using Application.IRepository;
using BusinessObject.DTOs.Account;
using Microsoft.AspNetCore.Mvc;

namespace HotelBookingAPI.Controllers
{
    [Route("api/account")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountRepository _accountRepository;
        public AccountController(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        [HttpGet()]
        public async Task<ActionResult<AccountViewDTO>> GetAccountById(string accountId)
        {
            if (accountId == null)
            {
                return BadRequest();
            }

            AccountViewDTO account = await _accountRepository.GetAccountById(accountId);

            if(account == null) 
            { 
                return NotFound();
            }

            return Ok(account);
        }

        [HttpGet("get-accounts")]
        public async Task<ActionResult<List<AccountViewDTO>>> GetAccounts()
        {   
            var accounts = await _accountRepository.GetAccounts();
            if (!accounts.Any())
            {
                return NotFound(nameof(accounts));
            }
            return Ok(accounts);
        }

        [HttpPut("update-account")]
        public async Task<ActionResult<AccountViewDTO>> UpdateAccount(AccountUpdateDTO accountUpdate)
        {
            if (accountUpdate == null)
            {
                return NotFound();
            }

            var account = await _accountRepository.UpdateAccount(accountUpdate);
            return Ok(account);
        }

        [HttpDelete("delete-account")]
        public async Task<ActionResult<bool>> DeleteAccount(string accountId)
        {
            return await _accountRepository.DeleteAccount(accountId);
        }

        [HttpPost("create-account")]
        public async Task<ActionResult<AccountCreateDTO>> AddAccount(AccountCreateDTO accountCreate)
        {
            var account = await _accountRepository.AddAccount(accountCreate);
            return Ok(account);
        }

        [HttpPost("login")]
        public async Task<ActionResult<AccountViewDTO>> Login(LoginDTO login)
        {
            var account = await _accountRepository.Login(login);    
            if (account == null)
            {
                return Unauthorized();
            }
            return Ok(account);
        }
    }
}
