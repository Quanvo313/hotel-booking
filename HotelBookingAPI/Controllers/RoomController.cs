﻿using Application.IRepository;
using BusinessObject.DTOs.Room;
using BusinessObject.Model;
using Microsoft.AspNetCore.Mvc;


namespace HotelBookingAPI.Controllers
{
    [Route("api/[controller]")]
	[ApiController]
	public class RoomController : ControllerBase
	{
		private readonly IRoomRepository _roomRepository;

		public RoomController(IRoomRepository roomRepository)
		{
			_roomRepository = roomRepository;

		}
		// GET: api/room
		[HttpGet]
		public async Task<ActionResult<IEnumerable<RoomDTO>>> GetAllRoomDtos()
		{
			var rooms = await _roomRepository.GetAllRoomsAsync();
			return Ok(rooms);
		}

		// GET: api/room/{id}
		[HttpGet("{id}")]
		public async Task<ActionResult<RoomDTO>> GetRoomByIdDTO(string id)
		{
			var room = await _roomRepository.GetRoomByIdAsync(id);
			if (room == null)
			{
				return NotFound();
			}
			return Ok(room);
		}
		// POST: api/room
		[HttpPost]
		public async Task<ActionResult<RoomDTO>> CreateRoomDTO(RoomDTO roomDTO)
		{
			var hotel = MapToRoom(roomDTO);
			await _roomRepository.AddRoomAsync(hotel);
			return NoContent();

		}
		// DELETE: api/room/{id}
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteRoomDTO(string id)
		{
			await _roomRepository.DeleteRoomAsync(id);
			return NoContent();
		}

		// PUT: api/room/{id}
		[HttpPut("{id}")]
		public async Task<IActionResult> UpdateRoomDTO(string id, RoomDTO roomDTO)
		{
			if (id != roomDTO.RoomID)
			{
				return BadRequest("Room ID mismatch");
			}

			var room = MapToRoom(roomDTO);
			await _roomRepository.UpdateRoomAsync(room);
			return NoContent();

		}

		private Room MapToRoom(RoomDTO roomDTO)
		{
			return new Room
			{
				RoomId = roomDTO.RoomID,
				Name = roomDTO.Name,
				Status = roomDTO.Status,
				RoomTypeId = roomDTO.RoomTypeID,
				HotelId = roomDTO.HotelID
			};
		}

		[HttpGet("room-status")]
		public async Task<ActionResult<List<RoomDTO>>> SearchRoomByStatus(string status)
		{
            var st = await _roomRepository.SearchRoomByStatus(status);
                return Ok(new
                {
                    StatusCode = 200,
                    Result = true,
                    Message = $"Get room status successful!",
                    Data = st
                });
		}
	}
}
