﻿using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace HotelBookingAPI.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<HotelBookingContext>(opt =>
            {
                opt.UseSqlServer(configuration.GetConnectionString("DbConnect"));
            });   
            return services;
        }
    }
}
