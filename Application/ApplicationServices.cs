﻿using Application.Utils;
using Microsoft.Extensions.DependencyInjection;

namespace Application
{
    public static class ApplicationServices
    {
        public static IServiceCollection AddApplicationUtils(this IServiceCollection services)
        {   
            services.AddScoped<PasswordHashing>();  
            services.AddScoped<MappingProfileService>();
            services.AddAutoMapper(cfg =>
            {
                var serviceProvider = services.BuildServiceProvider();
                var mappingProfileService = serviceProvider.GetRequiredService<MappingProfileService>();
                cfg.AddProfile(mappingProfileService);
            });
            
            return services;
        }

    }
}
