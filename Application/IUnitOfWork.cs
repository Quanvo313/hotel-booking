﻿using Application.IRepository;
namespace Application
{
    public interface IUnitOfWork 
    {
        IAccountRepository AccountRepository { get; }
        IHotelRepository HotelRepository { get; }
        IPaymentRepository PaymentRepository { get; }
        IRoomTypeRepository RoomTypeRepository { get; }
        IRoomRepository RoomRepository { get; }
        IUserRepository UserRepository { get; }
        IBookedDetailRepository BookedDetailRepository { get; }
    }
}
