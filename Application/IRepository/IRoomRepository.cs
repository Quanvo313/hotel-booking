﻿using BusinessObject.DTOs.Room;
using BusinessObject.Model;


namespace Application.IRepository
{
    public interface IRoomRepository
	{
		Task<IEnumerable<Room>> GetAllRoomsAsync();
		Task<Room> GetRoomByIdAsync(string id);
		Task AddRoomAsync(Room room);
		Task UpdateRoomAsync(Room room);
		Task DeleteRoomAsync(string roomId);
		Task<List<RoomDTO>> SearchRoomByStatus(string status);


    }
}
