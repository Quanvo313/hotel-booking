﻿using BusinessObject.DTOs;

namespace Application.IRepository
{
    public interface IUserRepository
    {
        Task<List<UserDTO>> GetListUser();
        Task<UserDTO> GetUserById(string id);
        Task<bool> DeleteUser(string id);
        Task<bool> UnDeleteUser(string id);
        Task<UserDTO> CreateUser(UserDTO userDTO);
        Task<UserDTO> UpdateUser(UserDTO userDTO);
        Task<string> GenetareId();

        Task<IEnumerable<UserDTO>> GetUsersByName(string nameParam);
        Task<IEnumerable<UserDTO>> GetUsersByEmail(string emailParam);
        Task<IEnumerable<UserDTO>> GetUsersByPhone(string phoneParam);
    }
}
