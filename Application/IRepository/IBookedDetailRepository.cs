﻿using BusinessObject.DTOs;

namespace Application.IRepository
{
    public interface IBookedDetailRepository
    {
        Task<List<BookedDetailDTO>> GetListBookedDetails();
        Task<BookedDetailDTO> GetBookedDetailsById(string id);
        Task<BookedDetailDTO> CreateBookedDetail(BookedDetailDTO bookedDetailDTO);
        Task<BookedDetailDTO> UpdateBookedDetail(BookedDetailDTO bookedDetailDTO);
        Task<bool> DeleteBookedDetail(string id);
        Task<bool> UnDeleteBookedDetail(string id);
    }
}
