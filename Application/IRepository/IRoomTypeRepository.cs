﻿using BusinessObject.DTOs;
using BusinessObject.Model;
namespace Application.IRepository
{
    public interface IRoomTypeRepository
    {
        Task<IEnumerable<RoomType>> getAll();
        Task<RoomType> getRoomTypeId(string roomTypeId);

        Task addRoomType(RoomType roomType);

        Task<RoomType> removeRoomType(string roomTypeId);

        Task<RoomType> updateRoomType(RoomType roomType);

        Task<RoomTypeDTO> GetRoomByRTName(string roomTypeName);
    }
}
