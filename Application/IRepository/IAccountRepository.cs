﻿using BusinessObject.DTOs.Account;

namespace Application.IRepository
{
    public interface IAccountRepository
    {
        Task<List<AccountViewDTO>> GetAccounts();
        Task<AccountViewDTO> GetAccountById(string id);
        Task<AccountCreateDTO> AddAccount(AccountCreateDTO account);
        Task<AccountViewDTO> UpdateAccount(AccountUpdateDTO account);
        Task<bool> DeleteAccount(string accountId);
        Task<AccountViewDTO> Login(LoginDTO login);
    }
}
