using BusinessObject.DTOs.Hotel;
using BusinessObject.Model;

namespace Application.IRepository
{
    public interface IHotelRepository
    {
        Task<IEnumerable<Hotel>> GetAllHotelsAsync();
        Task<Hotel> GetHotelByIdAsync(string id);
        Task AddHotelAsync(HotelCreateDTO hotel);
        Task UpdateHotelAsync(Hotel hotel);
        Task DeleteHotelAsync(string hotelId);
        Task<List<HotelDTO>> GetHotelByName(string name);   
        Task<List<HotelDTO>> SearchHotelByAddress(string searchAddress);
    }
}
