﻿using BusinessObject.DTOs.Booking;

namespace Application.IRepository
{
    public interface IBookingRepository
    {
        Task<List<BookingDTO>> GetAll();
        Task<BookingDTO> GetBookingById(string id);
        Task<BookingDTO> GetBookingByUserId(string id);
        Task<BookingCreateDTO> CreateBooking(BookingCreateDTO booking);
        Task<BookingDTO> UpdateBooking(BookingDTO booking);
        Task<bool> DeleteBooking(string id);
    }
}
