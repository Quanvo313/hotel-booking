﻿using BusinessObject.DTOs;

namespace Application.IRepository
{
    public interface IPaymentRepository 
    {
        Task<List<PaymentDTO>> GetAll();
        Task<PaymentDetailDTO> GetByID(string id);
        Task<IEnumerable<PaymentDTO>> GetByDate(DateTime date);
        Task<IEnumerable<PaymentDTO>> GetByDateRange(DateTime from, DateTime to);
        Task<PaymentCreateDTO> Insert(PaymentCreateDTO payment);
        Task<bool> Update(PaymentDetailDTO updatedPayment);
        Task<bool> Delete(string id);
    }
}
