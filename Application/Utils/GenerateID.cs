﻿namespace Application.Utils
{
    public class GenerateID
    {
        public string GenerateId(string prefix)
        {
            Random random = new Random();
            return $"{prefix}{random.NextInt64(1000, 9999)}";
        }

    }
}
