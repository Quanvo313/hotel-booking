﻿namespace Application.Utils.IUtils
{
    public interface IPasswordHashing
    {
        public byte[] GenerateSalt();
        public string HashPassword(string password, byte[] salt);
        public bool VerifyPassword(string password, string hashedPassword);
    }
}
