﻿using AutoMapper;
using BusinessObject.DTOs.Account;
using BusinessObject.DTOs;
using BusinessObject.Model;
using BusinessObject.DTOs.Hotel;
using BusinessObject.DTOs.BookedDetail;
using BusinessObject.DTOs.Booking;

namespace Application.Utils
{
    public class MappingProfileService : Profile
    {
        public MappingProfileService()
        {
            GenerateID generate = new GenerateID();
            PasswordHashing hashing = new PasswordHashing();
            //account
            CreateMap<AccountCreateDTO, Account>()
                .ForMember(dest => dest.AccountId, opt => opt.MapFrom(src => generate.GenerateId("AC")))
                .ForMember(dest => dest.Password, opt => opt.MapFrom(src => hashing.HashPassword(src.Password, hashing.GenerateSalt())));
            CreateMap<AccountUpdateDTO, Account>();
            CreateMap<Account, AccountViewDTO>().ReverseMap();
            CreateMap<LoginDTO, Account>();
            //payment
            CreateMap<PaymentCreateDTO, Payment>().ForMember(payment => payment.PaymentId, opt => opt.MapFrom(src => generate.GenerateId("PM")));
            CreateMap<PaymentDetailDTO, Payment>();
            CreateMap<PaymentDTO, Payment>();
            CreateMap<Payment, PaymentDTO>().ReverseMap();
            CreateMap<Payment, PaymentDetailDTO>().ReverseMap();
            //hotel
            CreateMap<HotelCreateDTO, Hotel>().ForMember(hotel => hotel.HotelId, opt => opt.MapFrom(src => generate.GenerateId("HT")));
            CreateMap<HotelDTO, Hotel>();
            CreateMap<Hotel, HotelCreateDTO>();
            //booking detail
            //CreateMap
            CreateMap<BookedDetailCreateDTO, BookedDetail>()
                .ForMember(dest => dest.IsDelete, opt => opt.MapFrom(src => false));
            //booking
            CreateMap<Booking, BookingCreateDTO>();
            CreateMap<BookingCreateDTO, Booking>()
                .ForMember(dest => dest.BookingId, opt => opt.MapFrom(src => generate.GenerateId("BK")))
                .ForMember(dest => dest.TotalAmount, opt => opt.MapFrom(src => 0));
        }


        public void CreateMappings()
        {
        }
    }
}
