﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.Hotel;
using HotelBooking_AdminView.IServices;
using HotelBooking_AdminView.Services;
using Microsoft.AspNetCore.Components;

namespace HotelBooking_AdminView.Pages
{
    public partial class UserManagement
    {
        [Inject]
        private IUserService userService { get; set; }
        private string message = "";
        private bool isError = false;
        private bool isPopUpVisible = false;
        private string email;
        private string name;
        private string phoneNumber;

        private string selectedUserId;

        private List<UserDTO> users;

        protected async override Task OnInitializedAsync()
        {
            try
            {
                users = new List<UserDTO>();
                users = await userService.GetListUser();
            }
            catch (Exception)
            {
                isError = true;
            }
        }

        private void ShowPopUp(string userId)
        {
            selectedUserId = userId;
            isPopUpVisible = true;

            UserDTO selectedUser = users.FirstOrDefault(u => u.UserId == selectedUserId);

            if (selectedUser != null)
            {
                email = selectedUser.Email;
                name = selectedUser.Name;
                phoneNumber = selectedUser.PhoneNumber;
            }
        }

        private void HidePopUp()
        {
            isPopUpVisible = false;
        }

        public class User
        {
            public string UserId { get; set; }
            public string Email { get; set; }
            public string Name { get; set; }
            public string PhoneNumber { get; set; }
        }
    }
}
