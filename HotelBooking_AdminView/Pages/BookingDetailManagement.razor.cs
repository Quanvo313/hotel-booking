﻿using BusinessObject.DTOs;
using HotelBooking_AdminView.IServices;
using Microsoft.AspNetCore.Components;

namespace HotelBooking_AdminView.Pages
{
    public partial class BookingDetailManagement
    {
        [Inject] 
        
        private IBookDetailService bookDetailService {  get; set; }
        private bool isError = false;
        private bool isAddPopUpVisible = false;
        private DateTime checkInDate =new DateTime(DateTime.Now.Ticks, DateTimeKind.Unspecified);
        private DateTime checkOutDate = new DateTime(DateTime.Now.Ticks, DateTimeKind.Unspecified);
        private string bookingId;
        private string roomId;
        private decimal amount;
        //add
      

        private DateTime CheckInDate= new DateTime(DateTime.Now.Ticks, DateTimeKind.Unspecified);
        private DateTime CheckOutDate = new DateTime(DateTime.Now.Ticks, DateTimeKind.Unspecified);
        private string BookingId;
        private string RoomId;
        private decimal Amount;
        private bool IsDelete = false;
        private List<BookedDetailDTO> list_booked;


        protected  async override Task OnInitializedAsync()
        {
            try
            {
                list_booked = new List<BookedDetailDTO>();
                list_booked = await bookDetailService.GetListBookedDetails();
            }
            catch (Exception ex)
            {
                isError = true;
            }
        }
        private void ShowAddPopUp()
        {
            isAddPopUpVisible = true;
            checkInDate = new DateTime();
            checkOutDate = new DateTime();
            bookingId = string.Empty;
            amount = 0;
        }

       private async Task AddBookedHandle()
        {
            try
            {
                BookedDetailDTO bookedDetailDTO = new BookedDetailDTO(CheckInDate = CheckInDate, CheckOutDate = CheckOutDate, Amount = Amount, RoomId = RoomId, BookingId = BookingId, IsDelete);
                await bookDetailService.CreateBookedDetail(bookedDetailDTO);
            }catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
           
        }




    }
}
