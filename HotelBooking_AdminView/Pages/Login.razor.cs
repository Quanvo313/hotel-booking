﻿using BusinessObject.DTOs.Account;
using HotelBooking_AdminView.Services;
using Microsoft.AspNetCore.Components;
using HotelBooking_AdminView.IServices;


namespace HotelBooking_AdminView.Pages
{
    public partial class Login
    {
        [Inject]
        private IAccountService accountService { get; set; }
        [Inject]
        private NavigationManager navigationManager { get; set; }

        private string username;
        private string password;
        private string type = "password";
        private bool check = false;
        private string errorMessage;


        private void PasswordToggle()
        {
            check = !check;
            type = check ? "text" : "password";
        }

        private async Task PerformLogin()
        {
            var loginDto = new LoginDTO { Username = username, Password = password };
            var result = await accountService.Login(loginDto);

            if (result != null)
            {
                if (result.AccountId.StartsWith("AC"))
                {
                    errorMessage = "Access denied for this role.";
                    return;
                }
                else if (result.AccountId.StartsWith("AD"))
                {
                    navigationManager.NavigateTo("/home");
                }
                else
                {
                    navigationManager.NavigateTo("/user");
                }
            }
            else
            {
                errorMessage = "Invalid username or password.";
            }
        }

    }
}



