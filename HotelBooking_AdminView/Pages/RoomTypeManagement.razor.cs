﻿// ... (your existing using statements)

using BusinessObject.DTOs;
using BusinessObject.DTOs.Hotel;
using HotelBooking_AdminView.IServices;
using HotelBooking_AdminView.Services;
using Microsoft.AspNetCore.Components;
using System.Net;
using System.Runtime.Intrinsics.X86;
using System.Xml.Linq;

namespace HotelBooking_AdminView.Pages
{
    public partial class RoomTypeManagement
    {
        [Inject]
        private IRoomTypeSerivce roomTypeService { get; set; }

        private string message = "";
        private bool isError = false;
        private bool isPopUpVisible = false;
        private bool isAddPopUpVisible = false;
        private string roomTypeName;
        private string image;
        private string description;
        private decimal basePrice;

        private List<RoomTypeDTO> roomTypes;

        private string selectedRoomTypeId;

        //add
        private string InputName;
        private string InputImage;
        private decimal InputBasePrice;
        private string InputDescription;

        protected async override Task OnInitializedAsync()
        {
            try
            {
                roomTypes = new List<RoomTypeDTO>();
                roomTypes = await roomTypeService.getAll();
            }
            catch (Exception)
            {
                isError = true;
            }
        }

        private void ShowPopUp(string roomTypeId)
        {
            selectedRoomTypeId = roomTypeId;
            isPopUpVisible = true;

            RoomTypeDTO selectedRoomType = roomTypes.FirstOrDefault(rt => rt.RoomTypeId == selectedRoomTypeId);

            if (selectedRoomType != null)
            {
                roomTypeName = selectedRoomType.RoomTypeName;
                image = selectedRoomType.Image;
                description = selectedRoomType.Description;
                basePrice = selectedRoomType.BasePrice;
            }
        }

        private void ShowAddPopUp()
        {
            isAddPopUpVisible = true;
            roomTypeName = string.Empty;
            image = string.Empty;
            description = string.Empty;
            basePrice = 0;
        }

        private void HideEditPopUp()
        {
            isPopUpVisible = false;
        }

        private void HideAddPopUp()
        {
            isAddPopUpVisible = false;
        }

        private async Task AddRoomTypeHandle()
        {
            await roomTypeService.addRoomType(new RoomTypeDTO { RoomTypeName = InputName, Image = InputImage, Description = InputDescription, BasePrice = InputBasePrice });
            message = "Add thành công";
            HideAddPopUp();
        }

        public class RoomType
        {
            public string RoomTypeId { get; set; }
            public string RoomTypeName { get; set; }
            public string Image { get; set; }
            public string Description { get; set; }
            public decimal BasePrice { get; set; }
        }

        private void SubmitForm()
        {
            HideEditPopUp();
        }

        private void DeleteRoomType(int roomTypeId)
        {

        }
    }
}
