﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.Hotel;
using BusinessObject.DTOs.Room;
using HotelBooking_AdminView.IServices;
using HotelBooking_AdminView.Services;
using Microsoft.AspNetCore.Components;

namespace HotelBooking_AdminView.Pages
{
    public partial class RoomManagement
    {
        [Inject]
        private IRoomService roomService { get; set; }

        [Inject]
        private IRoomTypeSerivce roomTypeService { get; set; }

        [Inject]
        private IHotelService hotelService { get; set; }


        private string message = "";
        private bool isError = false;
        private bool isPopUpVisible = false;
        private bool isAddPopUpVisible = false;
        private string name;
        private string status;
        private string roomtype;
        private string hotel;

        private string selectedRoomId;

        //add
        private string InputName;
        private string InputStatus;
        private string InputRoomType;
        private string InputHotel;


        private List<RoomTypeDTO> roomTypes;

        private List<RoomDTO> rooms;

        private List<HotelDTO> hotels;

        protected async override Task OnInitializedAsync()
        {
            try
            {
                rooms = new List<RoomDTO>();
                roomTypes = new List<RoomTypeDTO>();
                hotels = new List<HotelDTO>();
                rooms = await roomService.GetAllRoomsAsync();
                roomTypes = await roomTypeService.getAll();
                hotels = await hotelService.GetAllHotelsAsync();

            }
            catch (Exception)
            {
                isError = true;
            }
        }

        private void ShowPopUp(string roomId)
        {
            selectedRoomId = roomId;
            isPopUpVisible = true;

            RoomDTO selectedRoom = rooms.FirstOrDefault(r => r.RoomID == selectedRoomId);

            if (selectedRoom != null)
            {
                name = selectedRoom.Name;
                status = selectedRoom.Status;
                roomtype = selectedRoom.RoomTypeID;
                hotel = selectedRoom.HotelID;
            }
        }

        private void ShowAddPopUp()
        {
            isAddPopUpVisible = true;
            name = string.Empty;
            status = string.Empty;
            roomtype = string.Empty;
            hotel = string.Empty;
        }

        private void HideEditPopUp()
        {
            isPopUpVisible = false;
        }

        private void HideAddPopUp()
        {
            isAddPopUpVisible = false;
        }

        private async Task AddRoomHandle()
        {
            await roomService.AddRoomAsync(new RoomDTO { Name = InputName, Status = InputStatus, RoomTypeID = InputRoomType, HotelID = InputHotel });
            message = "Add thành công";
            HideAddPopUp();
        }

        public class Room
        {
            public string RoomId { get; set; }
            public string Name { get; set; }
            public string Status { get; set; }
            public string RoomType { get; set; }
            public string Hotel { get; set; }
        }

        public class RoomType
        {
            public string RoomTypeId { get; set; }
            public string RoomTypeName { get; set; }
        }

        public class Hotel
        {
            public string HotelId { get; set; }
            public string Name { get; set; }
        }
    }
}
