﻿using BusinessObject.DTOs.Account;
using BusinessObject.DTOs.Hotel;
using HotelBooking_AdminView.IServices;
using HotelBooking_AdminView.Services;
using Microsoft.AspNetCore.Components;
using System.Net.Http;
using System.Net.Http.Json;

namespace HotelBooking_AdminView.Pages
{
    public partial class HotelManagement
    {
        [Inject]
        private IHotelService hotelService { get; set; }

        private string message = "";
        private bool isError = false;
        private bool isPopUpVisible = false;
        private bool isAddPopUpVisible = false;
        private string address;
        private string name;
        private string image;
        private double avg;
        private string description;

        private string selectedHotelId;

        //add
        private string InputAddress;
        private string InputName;
        private string InputImage;
        private double InputAVG;
        private string InputDescription;


        private List<HotelDTO> hotels;

        protected async override Task OnInitializedAsync()
        {
            try
            {
                hotels = new List<HotelDTO>();
                hotels = await hotelService.GetAllHotelsAsync();
            }
            catch (Exception)
            {
                isError = true;
            }
        }


        private void ShowPopUp(string hotelId)
        {
            selectedHotelId = hotelId;
            isPopUpVisible = true;

            HotelDTO selectedHotel = hotels.FirstOrDefault(h => h.HotelId == selectedHotelId);

            if (selectedHotel != null)
            {
                address = selectedHotel.Address;
                name = selectedHotel.Name;
                image = selectedHotel.Image;
                avg = selectedHotel.AverageRating;
                description = selectedHotel.Decription;
            }
        }

        private void ShowAddPopUp()
        {
            isAddPopUpVisible = true;
            address = string.Empty;
            name = string.Empty;
            image = string.Empty;
            avg = 0f;
            description = string.Empty;
        }

        private void HideEditPopUp()
        {
            isPopUpVisible = false;
        }

        private void HideAddPopUp()
        {
            isAddPopUpVisible = false;
        }

        private async Task AddHotelHandle()
        {
            await hotelService.AddHotelAsync(new HotelCreateDTO { Address = InputAddress, Name = InputName, Image = InputImage, AverageRating = InputAVG, Decription = InputDescription});
            message = "Add thành công";
            HideAddPopUp();
        }

        private void SubmitForm()
        {
            HideEditPopUp();
        }

        private void DeleteHotel(int hotelId)
        {
            
        }

        public class Hotel
        {
            public string HotelId { get; set; }
            public string Address { get; set; }
            public string Name { get; set; }
            public string Image { get; set; }
            public double AverageRating { get; set; }
            public string Description { get; set; }
        }
    }
}
