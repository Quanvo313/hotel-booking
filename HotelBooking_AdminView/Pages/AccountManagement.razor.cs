﻿using BusinessObject.DTOs.Account;
using HotelBooking_AdminView.IServices;
using Microsoft.AspNetCore.Components;

namespace HotelBooking_AdminView.Pages
{
    public partial class AccountManagement
    {
        [Inject]   
        private IAccountService accountService { get; set; }

        private string message = "";
        private bool isError = false;
        private bool isPopUpVisible = false;
        private bool isAddPopUpVisible = false;
        private string username;
        private string password;
        private string confirmPassword;

        //add
        private string InputUsername;
        private string InputPassword;
        private string selectedAccountId;

        private List<AccountViewDTO> accounts;

        protected async override Task OnInitializedAsync()
        {   
            try
            {   
                accounts = new List<AccountViewDTO>();
                accounts = await accountService.GetAllAccount();
            }
            catch (Exception)
            {
                isError = true;
            }
        }


        private void ShowPopUp(string accountId)
        {
            selectedAccountId = accountId;
            isPopUpVisible = true;

            AccountViewDTO selectedAccount = accounts.FirstOrDefault(a => a.AccountId == selectedAccountId);

            if (selectedAccount != null)
            {
                username = selectedAccount.Username;
                password = selectedAccount.Password;
                confirmPassword = "";
            }
        }

        private void ShowAddPopUp()
        {
            isAddPopUpVisible = true;
            username = string.Empty;
            password = string.Empty;
            confirmPassword = string.Empty;
        }

        private async Task SaveAccount()
        {
           
        }


        private async Task DeleteAccount(string accountId)
        {
            var isDeleted = await accountService.DeleteAccount(accountId);
            if (isDeleted)
            {
                accounts.RemoveAll(a => a.AccountId == accountId);
            }
        }

        private void HideEditPopUp()
        {
            isPopUpVisible = false;
        }

        private void HideAddPopUp()
        {
            isAddPopUpVisible = false;
        }

        private async Task AddAccountHandle()
        {
            string PasswordConfirm = "haha";
            //if (!PasswordConfirm.Equals(InputPassword))
            //{
            //    message = "2 mật khẩu hong khớp";
            //    return;
            //}
            await accountService.AddAccount(new AccountCreateDTO { Username = InputUsername, Password = InputPassword });
            message = "Add thành công";
            HideAddPopUp(); 
        }
        public class Account
        {
            public int Id { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
        }
    }
}
