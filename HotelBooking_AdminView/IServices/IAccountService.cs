﻿using BusinessObject.DTOs.Account;

namespace HotelBooking_AdminView.IServices
{
    public interface IAccountService
    {
        Task<List<AccountViewDTO>> GetAllAccount();
        Task<AccountCreateDTO> AddAccount(AccountCreateDTO account);
        Task<bool> DeleteAccount(string accountId);
        Task<AccountViewDTO> GetAccountById(string id);
        Task<AccountViewDTO> UpdateAccount(AccountUpdateDTO account);
        Task<AccountViewDTO> Login(LoginDTO login);
    }
}
