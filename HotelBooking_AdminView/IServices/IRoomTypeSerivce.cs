﻿using BusinessObject.DTOs;

namespace HotelBooking_AdminView.IServices
{
    public interface IRoomTypeSerivce
    {
        Task<List<RoomTypeDTO>> getAll();
        Task<RoomTypeDTO> getRoomTypeId(string roomTypeId);

        Task<RoomTypeDTO> addRoomType(RoomTypeDTO roomType);

        Task<RoomTypeDTO> removeRoomType(string roomTypeId);

        Task<RoomTypeDTO> updateRoomType(RoomTypeDTO roomType);

        Task<RoomTypeDTO> GetRoomByRTName(string roomTypeName);
    }
}
