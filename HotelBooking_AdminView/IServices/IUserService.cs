﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.Hotel;

namespace HotelBooking_AdminView.IServices
{
    public interface IUserService
    {
        Task<List<UserDTO>> GetListUser();
        Task<UserDTO> GetUserById(string id);
        Task<bool> DeleteUser(string id);
        Task<bool> UnDeleteUser(string id);
        Task<UserDTO> CreateUser(UserDTO user);
        Task<UserDTO> UpdateUser(UserDTO user);
    }
}
