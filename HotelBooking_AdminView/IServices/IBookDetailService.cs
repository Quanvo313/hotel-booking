﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.BookedDetail;

namespace HotelBooking_AdminView.IServices
{
    public interface IBookDetailService
    {
        Task<List<BookedDetailDTO>> GetListBookedDetails();
        Task<BookedDetailDTO> GetBookedDetailsById(string id);
        Task<BookedDetailDTO> CreateBookedDetail(BookedDetailDTO bookedDetailDTO);
        Task<BookedDetailDTO> UpdateBookedDetail(BookedDetailDTO bookedDetailDTO);
        Task<bool> DeleteBookedDetail(string id);
        Task<bool> UnDeleteBookedDetail(string id);
    }
}
