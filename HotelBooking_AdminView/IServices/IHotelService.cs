﻿using BusinessObject.DTOs.Hotel;

namespace HotelBooking_AdminView.IServices
{
    public interface IHotelService
    {
        Task<List<HotelDTO>> GetAllHotelsAsync();
        Task<HotelCreateDTO> AddHotelAsync(HotelCreateDTO hotel);
        Task<bool> DeleteHotel(string hotelId);
        Task<HotelDTO> GetHotelById(string id);
        Task<HotelCreateDTO> UpdateHotel(HotelCreateDTO hotel);
    }
}
