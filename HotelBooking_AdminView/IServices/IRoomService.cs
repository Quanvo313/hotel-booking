﻿using BusinessObject.DTOs.Room;
using BusinessObject.Model;

namespace HotelBooking_AdminView.IServices
{
    public interface IRoomService
    {
        Task<List<RoomDTO>> GetAllRoomsAsync();
        Task<RoomDTO> GetRoomByIdAsync(string id);
        Task<RoomDTO> AddRoomAsync(RoomDTO room);
        Task<RoomDTO> UpdateRoomAsync(RoomDTO room);
        Task<RoomDTO> DeleteRoomAsync(string roomId);
        Task<List<RoomDTO>> SearchRoomByStatus(string status);
    }
}
