using HotelBooking_AdminView;
using HotelBooking_AdminView.IServices;
using HotelBooking_AdminView.Services;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");
builder.Services.AddScoped<IAccountService, AccountService>();
builder.Services.AddScoped<IHotelService, HotelService>();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IRoomService, RoomService>();
builder.Services.AddScoped<IRoomTypeSerivce, RoomTypeService>();
builder.Services.AddScoped<IBookDetailService, BookDetailService>();

await builder.Build().RunAsync();
