﻿using BusinessObject.DTOs.Account;
using HotelBooking_AdminView.IServices;
using System.Net.Http.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace HotelBooking_AdminView.Services
{
    public class AccountService : IAccountService
    {
        private HttpClient _httpClient;

        public AccountService()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("https://localhost:7158/");
        }

        public async Task<List<AccountViewDTO>> GetAllAccount()
        {
            var response = await _httpClient.GetFromJsonAsync<List<AccountViewDTO>>("api/account/get-accounts");
            return response;
        }

        public async Task<AccountCreateDTO> AddAccount(AccountCreateDTO account)
        {
            var response = await _httpClient.PostAsJsonAsync("api/account/create-account", account);
            return await response.Content.ReadFromJsonAsync<AccountCreateDTO>();
        }

        public async Task<bool> DeleteAccount(string accountId)
        {
            var response = await _httpClient.DeleteAsync($"api/account/delete-account/{accountId}");
            return response.IsSuccessStatusCode;
        }

        public async Task<AccountViewDTO> GetAccountById(string id)
        {
            var response = await _httpClient.GetFromJsonAsync<AccountViewDTO>($"api/account/{id}");
            return response;
        }

        public async Task<AccountViewDTO> UpdateAccount(AccountUpdateDTO account)
        {
            var response = await _httpClient.PutAsJsonAsync("api/account/update-account", account);
            return await response.Content.ReadFromJsonAsync<AccountViewDTO>();
        }

        public async Task<AccountViewDTO> Login(LoginDTO login)
        {
            var response = await _httpClient.PostAsJsonAsync("api/account/login", login);
            return await response.Content.ReadFromJsonAsync<AccountViewDTO>();
        }

    }
}