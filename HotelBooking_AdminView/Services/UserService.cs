﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.Hotel;
using HotelBooking_AdminView.IServices;
using System.Net.Http.Json;

namespace HotelBooking_AdminView.Services
{
    public class UserService : IUserService
    {
        private readonly HttpClient _httpClient;

        public UserService()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("https://localhost:7158/");
        }
        public async Task<List<UserDTO>> GetListUser()
        {
            var response = await _httpClient.GetFromJsonAsync<List<UserDTO>>("api/User/get-all-user");
            return response;
        }
        public async Task<UserDTO> GetUserById(string id)
        {
            var response = await _httpClient.GetFromJsonAsync<UserDTO>($"api/User/get-id-user/{id}");
            return response;
        }

        public Task<UserDTO> CreateUser(UserDTO user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteUser(string id)
        {
            throw new NotImplementedException();
        }



        public Task<bool> UnDeleteUser(string id)
        {
            throw new NotImplementedException();
        }

        public Task<UserDTO> UpdateUser(UserDTO user)
        {
            throw new NotImplementedException();
        }
    }
}
