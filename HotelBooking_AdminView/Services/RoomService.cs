﻿using BusinessObject.DTOs.Hotel;
using BusinessObject.DTOs.Room;
using HotelBooking_AdminView.IServices;
using System.Net.Http.Json;

namespace HotelBooking_AdminView.Services
{
    public class RoomService : IRoomService
    {
        private readonly HttpClient _httpClient;

        public RoomService()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("https://localhost:7158/");
        }

        public async Task<List<RoomDTO>> GetAllRoomsAsync()
        {
            var response = await _httpClient.GetFromJsonAsync<List<RoomDTO>>("api/Room");
            return response;
        }
        public async Task<RoomDTO> GetRoomByIdAsync(string id)
        {
            var response = await _httpClient.GetFromJsonAsync<RoomDTO>($"api/Room/{id}");
            return response;
        }
        public async Task<RoomDTO> AddRoomAsync(RoomDTO room)
        {
            var response = await _httpClient.PostAsJsonAsync("/api/Room", room);
            return await response.Content.ReadFromJsonAsync<RoomDTO>();
        }

        public Task<RoomDTO> DeleteRoomAsync(string roomId)
        {
            throw new NotImplementedException();
        }

        public Task<List<RoomDTO>> SearchRoomByStatus(string status)
        {
            throw new NotImplementedException();
        }

        public Task<RoomDTO> UpdateRoomAsync(RoomDTO room)
        {
            throw new NotImplementedException();
        }
    }
}
