﻿using BusinessObject.DTOs;
using BusinessObject.DTOs.Hotel;
using HotelBooking_AdminView.IServices;
using System.Net.Http.Json;

namespace HotelBooking_AdminView.Services
{
    public class RoomTypeService : IRoomTypeSerivce
    {
        private readonly HttpClient _httpClient;

        public RoomTypeService()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("https://localhost:7158/");
        }
        public async Task<List<RoomTypeDTO>> getAll()
        {
            var response = await _httpClient.GetFromJsonAsync<List<RoomTypeDTO>>("/api/RoomType/get-all");
            return response;
        }
        public async Task<RoomTypeDTO> getRoomTypeId(string roomTypeId)
        {
            var response = await _httpClient.GetFromJsonAsync<RoomTypeDTO>($"api/RoomType/get-by-id/{roomTypeId}");
            return response;
        }
        public async Task<RoomTypeDTO> addRoomType(RoomTypeDTO roomType)
        {
            var response = await _httpClient.PostAsJsonAsync("/api/RoomType", roomType);
            return await response.Content.ReadFromJsonAsync<RoomTypeDTO>();
        }


        public Task<RoomTypeDTO> GetRoomByRTName(string roomTypeName)
        {
            throw new NotImplementedException();
        }


        public Task<RoomTypeDTO> removeRoomType(string roomTypeId)
        {
            throw new NotImplementedException();
        }

        public Task<RoomTypeDTO> updateRoomType(RoomTypeDTO roomType)
        {
            throw new NotImplementedException();
        }
    }
}
