﻿using HotelBooking_AdminView.IServices;
using BusinessObject.DTOs;
using System.Net.Http.Json;

namespace HotelBooking_AdminView.Services
{
    public class BookDetailService : IBookDetailService
    {
        private readonly HttpClient _httpClient;

        public BookDetailService() {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("https://localhost:7158/");
        }

        public async Task<List<BookedDetailDTO>> GetListBookedDetails()
        {
            var response = await _httpClient.GetFromJsonAsync<List<BookedDetailDTO>>("/api/bookeddetail/get-all");
            return response;
        }

        public async Task<BookedDetailDTO> GetBookedDetailsById(string id)
        {
            var response = await _httpClient.GetFromJsonAsync<BookedDetailDTO>($"api/bookeddetail/get-id/{id}");
            return response;
        }

        public async Task<BookedDetailDTO> CreateBookedDetail(BookedDetailDTO bookedDetail)
        {
           
                var response = await _httpClient.PostAsJsonAsync("/api/bookeddetail/add", bookedDetail);
                return await response.Content.ReadFromJsonAsync<BookedDetailDTO>();
           
           
        }

        public async Task<bool> DeleteBookedDetail(string id)
        {
            var response = await _httpClient.DeleteAsync($"/api/bookeddetail/{id}");
            return response.IsSuccessStatusCode;
        }

        public async Task<BookedDetailDTO> UpdateBookedDetail(BookedDetailDTO id)
        {
            var response = await _httpClient.PutAsJsonAsync($"/api/bookeddetail/{id.BookingId}", id);

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadFromJsonAsync<BookedDetailDTO>();
        }


        public Task<bool> UnDeleteBookedDetail(string id)
        {
            throw new NotImplementedException();
        }

    }
}
