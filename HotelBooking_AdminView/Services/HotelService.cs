﻿using BusinessObject.DTOs.Account;
using BusinessObject.DTOs.Hotel;
using HotelBooking_AdminView.IServices;
using System.Net.Http.Json;

namespace HotelBooking_AdminView.Services
{
    public class HotelService : IHotelService
    {
        private readonly HttpClient _httpClient;

        public HotelService()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("https://localhost:7158/");
        }
        public async Task<List<HotelDTO>> GetAllHotelsAsync()
        {
            var response = await _httpClient.GetFromJsonAsync<List<HotelDTO>>("api/Hotel");
            return response;
        }

        public async Task<HotelCreateDTO> AddHotelAsync(HotelCreateDTO hotel)
        {
            var response = await _httpClient.PostAsJsonAsync("/api/Hotel", hotel);
            return await response.Content.ReadFromJsonAsync<HotelCreateDTO>();
        }

        public async Task<bool> DeleteHotel(string hotelId)
        {
            var response = await _httpClient.DeleteAsync($"/api/Hotel/{hotelId}");
            return response.IsSuccessStatusCode;
        }


        public async Task<HotelDTO> GetHotelById(string id)
        {
            var response = await _httpClient.GetFromJsonAsync<HotelDTO>($"api/Hotel/{id}");
            return response;
        }

        public Task<HotelCreateDTO> UpdateHotel(HotelCreateDTO hotel)
        {
            throw new NotImplementedException();
        }
    }
} 
