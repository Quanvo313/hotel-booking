﻿use master
go

drop database if exists hotel_booking
go

create database hotel_booking
go

use hotel_booking
go

CREATE TRIGGER tr_UpdateAverageRating
ON Reviews
AFTER INSERT
AS
BEGIN
    DECLARE @newAverageRating FLOAT;
    DECLARE @hotelID VARCHAR(6);
    
    -- Get the hotel ID from the inserted review
    SELECT @hotelID = HotelID FROM INSERTED;
    
    -- Calculate the new average rating for the hotel
    SELECT @newAverageRating = AVG(Rating)
    FROM Reviews
    WHERE HotelID = @hotelID AND IsDelete = 0;

    -- Update the average rating in the Hotel table
    UPDATE Hotel
    SET AverageRating = @newAverageRating
    WHERE HotelID = @hotelID;
END;

CREATE TRIGGER tr_UpdateRoomStatusToBooked
ON Bookings
AFTER UPDATE
AS
BEGIN
    DECLARE @bookingID VARCHAR(6);
    
    -- Check if the booking status has been updated to 'confirmed'
    IF EXISTS (SELECT 1 FROM INSERTED i JOIN DELETED d ON i.BookingID = d.BookingID 
               WHERE i.BookingStatus = 'confirmed' AND d.BookingStatus != 'confirmed')
    BEGIN
        SELECT @bookingID = BookingID FROM INSERTED;
        
        -- Update the status of the rooms associated with the booking to 'booked'
        UPDATE Rooms
        SET Status = 'booked'
        WHERE RoomID IN (SELECT RoomID FROM Booked_Detail WHERE BookingID = @bookingID);
    END
END;


CREATE TRIGGER tr_UpdateRoomStatusToVacant
ON Bookings
AFTER UPDATE
AS
BEGIN
    DECLARE @bookingID VARCHAR(6);
    
    -- Check if the booking status has been updated to 'cancelled'
    IF EXISTS (SELECT 1 FROM INSERTED i JOIN DELETED d ON i.BookingID = d.BookingID 
               WHERE i.BookingStatus = 'cancelled' AND d.BookingStatus != 'cancelled')
    BEGIN
        SELECT @bookingID = BookingID FROM INSERTED;
        
        -- Update the status of the rooms associated with the booking to 'vacant'
        UPDATE Rooms
        SET Status = 'vacant'
        WHERE RoomID IN (SELECT RoomID FROM Booked_Detail WHERE BookingID = @bookingID);
    END
END;


CREATE TRIGGER tr_CascadeDeleteToBookingsAndReviews
ON Users
AFTER UPDATE
AS
BEGIN
    DECLARE @userID VARCHAR(6);
    
    -- Check if the user is marked as deleted
    IF EXISTS (SELECT 1 FROM INSERTED i JOIN DELETED d ON i.UserID = d.UserID 
               WHERE i.IsDelete = 1 AND d.IsDelete = 0)
    BEGIN
        SELECT @userID = UserID FROM INSERTED;
        
        -- Mark related bookings and reviews as deleted
        UPDATE Bookings
        SET IsDelete = 1
        WHERE UserID = @userID;

        UPDATE Reviews
        SET IsDelete = 1
        WHERE ReviewID IN (SELECT ReviewID FROM Reviews WHERE HotelID IN 
                           (SELECT HotelID FROM Bookings WHERE UserID = @userID));
    END
END;


CREATE TRIGGER tr_CalculatePriceBasedOnDays
ON Booked_Detail
AFTER INSERT
AS
BEGIN
    DECLARE @bookingID VARCHAR(6);
    DECLARE @amount MONEY;
    DECLARE @basePrice MONEY;
    DECLARE @numberOfDays INT;

    -- Get the BookingID, RoomID, CheckInDate, and CheckOutDate from the inserted record
    DECLARE @roomID VARCHAR(6), @checkInDate DATETIME, @checkOutDate DATETIME;
    SELECT @bookingID = BookingID, @roomID = RoomID, @checkInDate = CheckInDate, @checkOutDate = CheckOutDate FROM INSERTED;

    -- Calculate the number of days
    SET @numberOfDays = DATEDIFF(DAY, @checkInDate, @checkOutDate);

    -- Fetch the base price of the room type
    SELECT @basePrice = BasePrice FROM RoomTypes rt
    JOIN Rooms r ON rt.RoomTypeID = r.RoomTypeID
    WHERE r.RoomID = @roomID;

    -- Calculate the amount
    SET @amount = @basePrice * @numberOfDays;

    -- Update the Amount in the Booked_Detail table for the current booking detail
    UPDATE Booked_Detail
    SET Amount = @amount
    WHERE BookingID = @bookingID AND RoomID = @roomID;

    -- Update the TotalAmount in the Bookings table
    UPDATE Bookings
    SET TotalAmount = TotalAmount + @amount
    WHERE BookingID = @bookingID;
END;



CREATE TABLE Accounts
(
  AccountID varchar(6) NOT NULL,
  Username varchar(20) NOT NULL,
  [Password] varchar(70) NOT NULL,
  IsDelete bit default(0),
  PRIMARY KEY (AccountID),
);
go


CREATE TABLE Users
(
  Email varchar(255) NOT NULL unique,
  Name varchar(200) NOT NULL,
  UserID varchar(6) NOT NULL,
  PhoneNumber varchar(10) NOT NULL,
  IsDelete bit default(0),
  AccountID varchar(6) foreign key references Accounts(AccountID)
  PRIMARY KEY (UserID)
);
go
CREATE TABLE Hotel
(
  HotelID varchar(6) NOT NULL,
  Address varchar(255) NOT NULL,
  Name varchar(100) NOT NULL,
  Image varchar(255) NOT NULL,
  AverageRating float NOT NULL default(5) check (AverageRating <= 5 ),
  Decription varchar(255) NOT NULL,
  IsDelete bit default(0),
  PRIMARY KEY (HotelID)
);
go
CREATE TABLE RoomTypes
(
  Image text,
  RoomTypeID varchar(6) NOT NULL,
  RoomTypeName varchar(6) NOT NULL,
  Description varchar(255) NOT NULL,
  BasePrice money NOT NULL,
  IsDelete bit default(0),
  PRIMARY KEY (RoomTypeID)
);
go
CREATE TABLE Rooms
(
  RoomID varchar(6) NOT NULL,
  [Name] varchar(6) NOT NULL,
  Status varchar(15) NOT NULL CHECK (Status IN ('booked', 'vacant', 'occupied')),
  RoomTypeID varchar(06) NOT NULL,
  HotelID varchar(6) NOT NULL,
  IsDelete bit default(0),
  PRIMARY KEY (RoomID),
  FOREIGN KEY (RoomTypeID) REFERENCES RoomTypes(RoomTypeID),
  FOREIGN KEY (HotelID) REFERENCES Hotel(HotelID)
);
go
CREATE TABLE Reviews
(
  ReviewID varchar(6) NOT NULL,
  Rating float NOT NULL check (Rating <= 5),
  Comment varchar(255) NOT NULL,
  HotelID varchar(6) NOT NULL,
  IsDelete bit default(0),
  PRIMARY KEY (ReviewID),
  FOREIGN KEY (HotelID) REFERENCES Hotel(HotelID)
);
go

CREATE TABLE Payments
(
  PaymentID varchar(6) NOT NULL,
  PaymentMethod varchar(20) NOT NULL,
  TotalPaymentAmount money NOT NULL,
  PaymentDate datetime NOT NULL,
  IsDelete bit default(0),
  PRIMARY KEY (PaymentID)
);
go
CREATE TABLE Bookings
(
  BookingID varchar(6) NOT NULL,
  BookingStatus varchar(10) NOT NULL CHECK (BookingStatus IN ('pending', 'confirmed', 'cancelled')),
  TotalAmount money default(0),
  PaymentID varchar(6) NOT NULL,
  UserID varchar(6) NOT NULL,
  IsDelete bit default(0),
  PRIMARY KEY (BookingID),
  FOREIGN KEY (PaymentID) REFERENCES Payments(PaymentID),
  FOREIGN KEY (UserID) REFERENCES Users(UserID)
);
go
CREATE TABLE Booked_Detail
(
  CheckInDate datetime NOT NULL,
  CheckOutDate datetime NOT NULL,
  Amount money default(0),
  RoomID varchar(6) NOT NULL,
  BookingID varchar(6) NOT NULL,
  IsDelete bit default(0),
  PRIMARY KEY (RoomID, BookingID),
  FOREIGN KEY (RoomID) REFERENCES Rooms(RoomID),
  FOREIGN KEY (BookingID) REFERENCES Bookings(BookingID),
  CHECK (CheckInDate < CheckOutDate)
);
go











select * from Users

select * from Hotel

select * from Accounts

select * from Bookings

select * from Rooms

select * from RoomTypes

select * from Booked_Detail

select * from Payments

select * from Reviews


-- Thêm dữ liệu vào bảng Accounts
INSERT INTO Accounts (AccountID, Username, [Password]) VALUES ('A00001', 'user1', 'password1');

-- Thêm dữ liệu vào bảng Users
INSERT INTO Users (Email, Name, UserID, PhoneNumber, AccountID) VALUES ('user1@example.com', 'User One', 'U00001', '1234567890', 'A00001');

-- Thêm dữ liệu vào bảng Hotel
INSERT INTO Hotel (HotelID, Address, Name, Image, Decription) VALUES ('H00001', '123 Main St', 'Hotel One', 'hotel_image.jpg', 'A nice hotel');

-- Thêm dữ liệu vào bảng RoomTypes
INSERT INTO RoomTypes (RoomTypeID, RoomTypeName, Description, BasePrice) VALUES ('RT0001', 'Single', 'A single room', 100);

-- Thêm dữ liệu vào bảng Rooms
INSERT INTO Rooms (RoomID, [Name], Status, RoomTypeID, HotelID) VALUES ('R00001', 'Room01', 'vacant', 'RT0001', 'H00001');
INSERT INTO Rooms (RoomID, [Name], Status, RoomTypeID, HotelID) VALUES ('R00002', 'Room02', 'vacant', 'RT0001', 'H00001');

-- Thêm dữ liệu vào bảng Reviews
INSERT INTO Reviews (ReviewID, Rating, Comment, HotelID) VALUES ('RV0001', 4.5, 'Great hotel!', 'H00001');

-- Thêm dữ liệu vào bảng Payments
INSERT INTO Payments (PaymentID, PaymentMethod, TotalPaymentAmount, PaymentDate) VALUES ('P00001', 'Credit Card', 150, GETDATE());

-- Thêm dữ liệu vào bảng Bookings
INSERT INTO Bookings (BookingID, BookingStatus, TotalAmount, PaymentID, UserID) VALUES ('B00001', 'confirmed', 150, 'P00001', 'U00001');

-- Thêm dữ liệu vào bảng Booked_Detail để kích hoạt trigger
INSERT INTO Booked_Detail (CheckInDate, CheckOutDate, RoomID, BookingID) VALUES ('2023-10-11', '2023-10-15', 'R00001', 'B00001');

INSERT INTO Booked_Detail (CheckInDate, CheckOutDate, RoomID, BookingID) VALUES ('2023-10-11', '2023-10-13', 'R00002', 'B00001');

INSERT [dbo].[Accounts] ([AccountID], [Username], [Password], [IsDelete]) VALUES (N'AD0000', N'admin', N'647S0irzBQdIZNYuxvubVw==:OapfuFb0+H2X6PIfOmt7n/ScG6NVutqj0TDd4IpyuZY=', 0)
INSERT [dbo].[Accounts] ([AccountID], [Username], [Password], [IsDelete]) VALUES (N'AC0001', N'usertest1', N'42vAU89tXDgc6Uc93W1Z4w==:/dfv5KjknSSzlKjDaKIRT0BbVchkffxcWAD3p5f1HNg=', 0)
INSERT [dbo].[Accounts] ([AccountID], [Username], [Password], [IsDelete]) VALUES (N'AC0002', N'usertest2', N'42vAU89tXDgc6Uc93W1Z4w==:/dfv5KjknSSzlKjDaKIRT0BbVchkffxcWAD3p5f1HNg=', 0)
INSERT [dbo].[Accounts] ([AccountID], [Username], [Password], [IsDelete]) VALUES (N'AC0003', N'usertest3', N'42vAU89tXDgc6Uc93W1Z4w==:/dfv5KjknSSzlKjDaKIRT0BbVchkffxcWAD3p5f1HNg=', 0)
INSERT [dbo].[Accounts] ([AccountID], [Username], [Password], [IsDelete]) VALUES (N'AC0004', N'usertest4', N'42vAU89tXDgc6Uc93W1Z4w==:/dfv5KjknSSzlKjDaKIRT0BbVchkffxcWAD3p5f1HNg=', 0)
INSERT [dbo].[Accounts] ([AccountID], [Username], [Password], [IsDelete]) VALUES (N'AC0005', N'usertest5', N'42vAU89tXDgc6Uc93W1Z4w==:/dfv5KjknSSzlKjDaKIRT0BbVchkffxcWAD3p5f1HNg=', 0)
go

INSERT [dbo].[Users] ([Email], [Name], [UserID], [PhoneNumber], [IsDelete], AccountID) VALUES (N'khiemph123@gmail.com', N'Khiem Huynh', N'U00001', N'1234567890', 0, 'AC0001')
INSERT [dbo].[Users] ([Email], [Name], [UserID], [PhoneNumber], [IsDelete], AccountID) VALUES (N'phongpdt@gmail.com', N'Phong', N'U00002', N'0987654321', 0, 'AC0002')
INSERT [dbo].[Users] ([Email], [Name], [UserID], [PhoneNumber], [IsDelete], AccountID) VALUES (N'khanhld@gmail.com', N'Le Duy Khanh', N'U00003', N'1234567890', 0, 'AC0003')
INSERT [dbo].[Users] ([Email], [Name], [UserID], [PhoneNumber], [IsDelete], AccountID) VALUES (N'quangpn@gmail.com', N'Quang Pham Nhat', N'U00004', N'0987654321', 0, 'AC0004')
INSERT [dbo].[Users] ([Email], [Name], [UserID], [PhoneNumber], [IsDelete], AccountID) VALUES (N'phatpt123@gmail.com', N'Phat Pham Tan', N'U00005', N'1234567890', 0, 'AC0005')
GO

INSERT [dbo].[Hotel] ([HotelID], [Address], [Name], [Image], [AverageRating], [Decription], [IsDelete]) VALUES (N'H00001', 'No 8, Do Duc Duc Street, Me Tri Ward, Nam Tu Liem District,Hanoi, Hanoi 100000', N'JW Marriott Hotel Hanoi Ha Noi', 'https://images.toplist.vn/images/800px/jw-marriott-hotel-hanoi-ha-noi-327700.jpg', 4.5, N'a premier luxury hotel offering world-class amenities and exceptional service in a stunning modern setting.', 0)
INSERT [dbo].[Hotel] ([HotelID], [Address], [Name], [Image], [AverageRating], [Decription], [IsDelete]) VALUES (N'H00002', '136 Hang Trong, Hoan Kiem Dist, Hanoi, Vietnam', N'Apricot Ha Noi Hotel', 'https://www.vietnambooking.com/wp-content/uploads/2020/12/Khach-San-Apricot-Ha-Noi.jpg', 3.9, N'is a boutique hotel in the heart of Hanoi, offering a blend of Vietnamese art, culture, and luxury accommodation for a memorable stay.', 0)
INSERT [dbo].[Hotel] ([HotelID], [Address], [Name], [Image], [AverageRating], [Decription], [IsDelete]) VALUES (N'H00003', '90 Le Duan Street, Ward 3, Tay Ninh City, Vietnam', N'Vinpearl Tay Ninh Hotel', 'https://www.vietnambooking.com/wp-content/uploads/2020/12/Khach-san-Vinpearl-Tay-Ninh.jpg', 4.9, N'This is an elegant oasis in Tay Ninh, Vietnam, known for its serene ambiance and exceptional hospitality.', 0)
INSERT [dbo].[Hotel] ([HotelID], [Address], [Name], [Image], [AverageRating], [Decription], [IsDelete]) VALUES (N'H00004', '720A Dien Bien Phu Street, Ward 22, Binh Thanh District, Ho Chi Minh City', N'Vinpearl Luxury Landmark 81 – Ho Chi Minh', 'https://cdn3.dhht.vn/wp-content/uploads/2022/08/bia-landmark-81-o-dau-an-gi-choi-gi-cac-goc-chup-hinh-dep.jpg', 4.2, N'This is a prestigious hotel offering unparalleled luxury and breathtaking views from the tallest skyscraper in Vietnam.', 0)
INSERT [dbo].[Hotel] ([HotelID], [Address], [Name], [Image], [AverageRating], [Decription], [IsDelete]) VALUES (N'H00005', 'E1 Area, Con Cai Khe, Cai Khe Ward, Ninh Kieu District, Can Tho City, Vietnam', N'Muong Thanh Luxury Hotel', 'https://muongthanh.com/images/brands/2019/10/11/original/luxury-nhat-le_1570769504.jpg', 3.8, N'This is a lavish retreat offering opulent accommodations and top-notch amenities.', 0)
GO


INSERT [dbo].[RoomTypes] ([Image], [RoomTypeID], [RoomTypeName], [Description], [BasePrice], [IsDelete]) VALUES (N'https://23c133e0c1637be1e07d-be55c16c6d91e6ac40d594f7e280b390.ssl.cf1.rackcdn.com/u/phhk/home/1a-Superior-Single-Room-min1.jpg', N'RT001', N'Single', N'Single bed room', 100.0000, 0)
INSERT [dbo].[RoomTypes] ([Image], [RoomTypeID], [RoomTypeName], [Description], [BasePrice], [IsDelete]) VALUES (N'https://chaniamorum.gr/wp-content/uploads/2021/04/DSC_1503ok.jpg', N'RT002', N'Double', N'Double bed room', 150.0000, 0)
INSERT [dbo].[RoomTypes] ([Image], [RoomTypeID], [RoomTypeName], [Description], [BasePrice], [IsDelete]) VALUES (N'https://media.cnn.com/api/v1/images/stellar/prod/140127103345-peninsula-shanghai-deluxe-mock-up.jpg?q=w_2226,h_1449,x_0,y_0,c_fill', N'RT003', N'Suite', N'Luxury Suite', 300.0000, 0)
INSERT [dbo].[RoomTypes] ([Image], [RoomTypeID], [RoomTypeName], [Description], [BasePrice], [IsDelete]) VALUES (N'https://castlehotel.net/wp-content/uploads/2017/06/057_CastleH-1200x504.jpg', N'RT004', N'Twin', N'Twin bed room', 120.0000, 0)
INSERT [dbo].[RoomTypes] ([Image], [RoomTypeID], [RoomTypeName], [Description], [BasePrice], [IsDelete]) VALUES (N'https://i.pinimg.com/564x/ed/c4/c6/edc4c6adf28bb29a48f140f92692d2cc.jpg', N'RT005', N'Queen', N'Queen bed room', 200.0000, 0)
GO

INSERT [dbo].[Rooms] ([RoomID], [Name], [Status], [RoomTypeID], [HotelID], [IsDelete]) VALUES (N'R0001', N'101', N'vacant', N'RT001', N'H00001', 0)
INSERT [dbo].[Rooms] ([RoomID], [Name], [Status], [RoomTypeID], [HotelID], [IsDelete]) VALUES (N'R0002', N'102', N'vacant', N'RT002', N'H00002', 0)
INSERT [dbo].[Rooms] ([RoomID], [Name], [Status], [RoomTypeID], [HotelID], [IsDelete]) VALUES (N'R0003', N'103', N'booked', N'RT003', N'H00003', 0)
INSERT [dbo].[Rooms] ([RoomID], [Name], [Status], [RoomTypeID], [HotelID], [IsDelete]) VALUES (N'R0004', N'104', N'occupied', N'RT004', N'H00004', 0)
INSERT [dbo].[Rooms] ([RoomID], [Name], [Status], [RoomTypeID], [HotelID], [IsDelete]) VALUES (N'R0005', N'105', N'vacant', N'RT005', N'H00005', 0)
GO

INSERT [dbo].[Payments] ([PaymentID], [PaymentMethod], [TotalPaymentAmount], [PaymentDate], [IsDelete]) VALUES (N'PM0001', N'Credit Card', 200.0000, CAST(N'2023-09-22T09:53:22.627' AS DateTime), 0)
INSERT [dbo].[Payments] ([PaymentID], [PaymentMethod], [TotalPaymentAmount], [PaymentDate], [IsDelete]) VALUES (N'PM0002', N'PayPal', 150.0000, CAST(N'2023-09-22T09:53:22.627' AS DateTime), 0)
INSERT [dbo].[Payments] ([PaymentID], [PaymentMethod], [TotalPaymentAmount], [PaymentDate], [IsDelete]) VALUES (N'PM0003', N'Debit Card', 100.0000, CAST(N'2023-09-22T09:53:22.627' AS DateTime), 0)
INSERT [dbo].[Payments] ([PaymentID], [PaymentMethod], [TotalPaymentAmount], [PaymentDate], [IsDelete]) VALUES (N'PM0004', N'Cash', 250.0000, CAST(N'2023-09-22T09:53:22.627' AS DateTime), 0)
INSERT [dbo].[Payments] ([PaymentID], [PaymentMethod], [TotalPaymentAmount], [PaymentDate], [IsDelete]) VALUES (N'PM0005', N'Google Pay', 120.0000, CAST(N'2023-09-22T09:53:22.627' AS DateTime), 0)
GO


INSERT [dbo].[Bookings] ([BookingID], [BookingStatus], [TotalAmount], [PaymentID], [UserID], [IsDelete]) VALUES (N'BK0001', N'confirmed', 200.0000, N'PM0001', N'U00001', 0)
INSERT [dbo].[Bookings] ([BookingID], [BookingStatus], [TotalAmount], [PaymentID], [UserID], [IsDelete]) VALUES (N'BK0002', N'pending', 150.0000, N'PM0002', N'U00002', 0)
INSERT [dbo].[Bookings] ([BookingID], [BookingStatus], [TotalAmount], [PaymentID], [UserID], [IsDelete]) VALUES (N'BK0003', N'cancelled', 100.0000, N'PM0003', N'U00003', 0)
INSERT [dbo].[Bookings] ([BookingID], [BookingStatus], [TotalAmount], [PaymentID], [UserID], [IsDelete]) VALUES (N'BK0004', N'pending', 250.0000, N'PM0004', N'U00004', 0)
INSERT [dbo].[Bookings] ([BookingID], [BookingStatus], [TotalAmount], [PaymentID], [UserID], [IsDelete]) VALUES (N'BK0005', N'pending', 120.0000, N'PM0005', N'U00005', 0)
GO

INSERT [dbo].[Booked_Detail] ([CheckInDate], [CheckOutDate], [Amount], [RoomID], [BookingID], [IsDelete]) VALUES (CAST(N'2023-09-22T09:53:22.627' AS DateTime), CAST(N'2023-09-24T09:53:22.627' AS DateTime), 100.0000, N'R0001', N'BK0001', 0)
INSERT [dbo].[Booked_Detail] ([CheckInDate], [CheckOutDate], [Amount], [RoomID], [BookingID], [IsDelete]) VALUES (CAST(N'2023-09-22T09:53:22.627' AS DateTime), CAST(N'2023-09-23T09:53:22.627' AS DateTime), 50.0000, N'R0002', N'BK0002', 0)
INSERT [dbo].[Booked_Detail] ([CheckInDate], [CheckOutDate], [Amount], [RoomID], [BookingID], [IsDelete]) VALUES (CAST(N'2023-09-22T09:53:22.627' AS DateTime), CAST(N'2023-09-25T09:53:22.627' AS DateTime), 70.0000, N'R0003', N'BK0003', 0)
INSERT [dbo].[Booked_Detail] ([CheckInDate], [CheckOutDate], [Amount], [RoomID], [BookingID], [IsDelete]) VALUES (CAST(N'2023-09-22T09:53:22.627' AS DateTime), CAST(N'2023-09-26T09:53:22.627' AS DateTime), 100.0000, N'R0004', N'BK0004', 0)
INSERT [dbo].[Booked_Detail] ([CheckInDate], [CheckOutDate], [Amount], [RoomID], [BookingID], [IsDelete]) VALUES (CAST(N'2023-09-22T09:53:22.627' AS DateTime), CAST(N'2023-09-27T09:53:22.627' AS DateTime), 80.0000, N'R0005', N'BK0005', 0)
INSERT [dbo].[Booked_Detail] ([CheckInDate], [CheckOutDate], [Amount], [RoomID], [BookingID], [IsDelete]) VALUES (CAST(N'2023-09-22T09:53:22.627' AS DateTime), CAST(N'2023-09-27T09:53:22.627' AS DateTime), 80.0000, N'R0003', N'BK0005', 0)
GO

INSERT [dbo].[Reviews] ([ReviewID], [Rating], [Comment], [HotelID], [IsDelete]) VALUES (N'RV0001', 4, N'Great service', N'H00001', 0)
INSERT [dbo].[Reviews] ([ReviewID], [Rating], [Comment], [HotelID], [IsDelete]) VALUES (N'RV0002', 3.5, N'Good for the price', N'H00002', 0)
INSERT [dbo].[Reviews] ([ReviewID], [Rating], [Comment], [HotelID], [IsDelete]) VALUES (N'RV0003', 4.2, N'Nice experience', N'H00003', 0)
INSERT [dbo].[Reviews] ([ReviewID], [Rating], [Comment], [HotelID], [IsDelete]) VALUES (N'RV0004', 2.8, N'Needs improvement', N'H00004', 0)
INSERT [dbo].[Reviews] ([ReviewID], [Rating], [Comment], [HotelID], [IsDelete]) VALUES (N'RV0005', 3.7, N'Satisfactory', N'H00005', 0)
GO