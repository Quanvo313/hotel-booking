﻿namespace BusinessObject.Model;

public partial class BookedDetail
{
    public DateTime CheckInDate { get; set; }

    public DateTime CheckOutDate { get; set; }

    public decimal Amount { get; set; }

    public string RoomId { get; set; } = null!;

    public string BookingId { get; set; } = null!;

    public bool? IsDelete { get; set; }

    public virtual Booking Booking { get; set; } = null!;

    public virtual Room Room { get; set; } = null!;
}
