﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Model;

public partial class RoomType
{
    public string? Image { get; set; }

    public string RoomTypeId { get; set; } = null!;

    public string RoomTypeName { get; set; } = null!;

    public string Description { get; set; } = null!;

    public decimal BasePrice { get; set; }

    public bool? IsDelete { get; set; }

    public virtual ICollection<Room> Rooms { get; set; } = new List<Room>();
}
