﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Model;

public partial class Hotel
{
    public string HotelId { get; set; } = null!;

    public string Address { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string Image { get; set; } = null!;

    public double AverageRating { get; set; }

    public string Decription { get; set; } = null!;

    public bool? IsDelete { get; set; }

    public virtual ICollection<Review> Reviews { get; set; } = new List<Review>();

    public virtual ICollection<Room> Rooms { get; set; } = new List<Room>();
}
