﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Model;

public partial class Payment
{
    public string PaymentId { get; set; } = null!;

    public string PaymentMethod { get; set; } = null!;

    public decimal TotalPaymentAmount { get; set; }

    public DateTime PaymentDate { get; set; }

    public bool? IsDelete { get; set; }

    public virtual ICollection<Booking> Bookings { get; set; } = new List<Booking>();
}
