﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Model;

public partial class Review
{
    public string ReviewId { get; set; } = null!;

    public double Rating { get; set; }

    public string Comment { get; set; } = null!;

    public string HotelId { get; set; } = null!;

    public string UserId { get; set; } = null!;

    public bool? IsDelete { get; set; }

    public virtual Hotel Hotel { get; set; } = null!;

    public virtual User User { get; set; } = null!;
}
