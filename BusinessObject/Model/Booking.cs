﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Model;

public partial class Booking
{
    public string BookingId { get; set; } = null!;

    public string BookingStatus { get; set; } = null!;

    public decimal TotalAmount { get; set; }

    public string PaymentId { get; set; } = null!;

    public string UserId { get; set; } = null!;

    public bool? IsDelete { get; set; }

    public virtual ICollection<BookedDetail> BookedDetails { get; set; } = new List<BookedDetail>();

    public virtual Payment Payment { get; set; } = null!;

    public virtual User User { get; set; } = null!;
}
