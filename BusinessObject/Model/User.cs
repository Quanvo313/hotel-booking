﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Model;

public partial class User
{
    public string Email { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string UserId { get; set; } = null!;

    public string PhoneNumber { get; set; } = null!;

    public bool? IsDelete { get; set; }

    public string? AccountId { get; set; }

    public virtual Account? Account { get; set; }

    public virtual ICollection<Booking> Bookings { get; set; } = new List<Booking>();

    public virtual ICollection<Review> Reviews { get; set; } = new List<Review>();
}
