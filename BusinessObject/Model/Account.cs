﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Model;

public partial class Account
{
    public string AccountId { get; set; } = null!;

    public string Username { get; set; } = null!;

    public string Password { get; set; } = null!;

    public bool? IsDelete { get; set; }

    public virtual ICollection<User> Users { get; set; } = new List<User>();
}
