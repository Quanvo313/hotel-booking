﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Model;

public partial class Room
{
    public string RoomId { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string Status { get; set; } = null!;

    public string RoomTypeId { get; set; } = null!;

    public string HotelId { get; set; } = null!;

    public bool? IsDelete { get; set; }

    public virtual ICollection<BookedDetail> BookedDetails { get; set; } = new List<BookedDetail>();

    public virtual Hotel Hotel { get; set; } = null!;

    public virtual RoomType RoomType { get; set; } = null!;
}
