﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs
{
    public class UserDTO
    {
        public string Email { get; set; } = null!;

        public string Name { get; set; } = null!;

        public string UserId { get; set; } = null!;

        public string PhoneNumber { get; set; } = null!;

        public bool? IsDelete { get; set; }
    }
}
