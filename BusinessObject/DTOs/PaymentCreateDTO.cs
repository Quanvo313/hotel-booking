﻿namespace BusinessObject.DTOs
{
    public class PaymentCreateDTO
    {

        public string PaymentMethod { get; set; } = null!;

        public decimal TotalPaymentAmount { get; set; }

        public DateTime PaymentDate { get; set; }
    }
}
