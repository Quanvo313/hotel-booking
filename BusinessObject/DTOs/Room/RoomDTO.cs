﻿namespace BusinessObject.DTOs.Room
{
    public class RoomDTO
    {
        public string RoomID { get; set; } = null!;
        public string Name { get; set; }
        public string Status { get; set; } = null!;
        public string RoomTypeID { get; set; } = null!;
        public string HotelID { get; set; } = null!;

    }
}
