﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.DTOs.Room
{
    public  class RoomCreateDTO
    {
        public string Name { get; set; }
        public string Status { get; set; } = null!;
        public string RoomTypeID { get; set; } = null!;
        public string HotelID { get; set; } = null!;
    }
}
