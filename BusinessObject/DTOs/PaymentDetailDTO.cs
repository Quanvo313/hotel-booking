﻿namespace BusinessObject.DTOs
{
    public class PaymentDetailDTO
    {
        public string PaymentId { get; set; } = null!;

        public string PaymentMethod { get; set; } = null!;

        public decimal TotalPaymentAmount { get; set; }

        public DateTime PaymentDate { get; set; }
    }
}
