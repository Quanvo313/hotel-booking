﻿namespace BusinessObject.DTOs.Hotel
{
    public class HotelCreateDTO
    {
        public string Address { get; set; }

        public string Name { get; set; }

        public string Image { get; set; }

        public double AverageRating { get; set; }

        public string Decription { get; set; } = null!;
    }
}
