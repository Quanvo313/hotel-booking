﻿namespace BusinessObject.DTOs.Account
{
    public class AccountViewDTO
    {
        public string AccountId { get; set; } = null!;

        public string Username { get; set; } = null!;

        public string Password { get; set; } = null!;

        public bool? IsDelete { get; set; }
    }
}
