﻿namespace BusinessObject.DTOs.Account
{
    public class AccountCreateDTO
    {
        public string Username { get; set; } = null!;
        public string Password { get; set; } = null!;
    }
}
