﻿namespace BusinessObject.DTOs.Account
{
    public class AccountUpdateDTO
    {
        public string AccountId { get; set; } = null!;

        public string Username { get; set; } = null!;

        public string Password { get; set; } = null!;
    }
}
