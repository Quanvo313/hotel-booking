﻿namespace BusinessObject.DTOs.Booking
{
    public class BookingDTO
    {
        public string BookingId { get; set; } = null!;

        public string BookingStatus { get; set; } = null!;

        public decimal TotalAmount { get; set; }

        public string PaymentId { get; set; } = null!;

        public string UserId { get; set; } = null!;

        public bool? IsDelete { get; set; }
    }
}
