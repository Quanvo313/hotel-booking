﻿namespace BusinessObject.DTOs.Booking
{
    public class BookingCreateDTO
    {
        public string BookingStatus { get; set; } = null!;

        public string PaymentId { get; set; } = null!;

        public string UserId { get; set; } = null!;

        public bool? IsDelete { get; set; }
    }
}
