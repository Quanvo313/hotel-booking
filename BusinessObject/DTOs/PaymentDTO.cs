﻿namespace BusinessObject.DTOs
{
    public class PaymentDTO
    {
        public string PaymentId { get; set; } = null!;

        public string PaymentMethod { get; set; } = null!;

        public DateTime PaymentDate { get; set; }
    }
}
