﻿namespace BusinessObject.DTOs
{
    public class RoomTypeDTO
    {
        public string Image { get; set; }

        public string RoomTypeId { get; set; } = null!;

        public string RoomTypeName { get; set; } = null!;

        public string Description { get; set; } = null!;

        public decimal BasePrice { get; set; }

    }
}
