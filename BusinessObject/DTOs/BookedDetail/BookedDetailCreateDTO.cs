﻿namespace BusinessObject.DTOs.BookedDetail
{
    public class BookedDetailCreateDTO
    {
        public DateTime CheckInDate { get; set; }

        public DateTime CheckOutDate { get; set; }

        public decimal Amount { get; set; }

        public string RoomId { get; set; } = null!;

        public string BookingId { get; set; } = null!;

        public bool? IsDelete { get; set; }
    }
}
