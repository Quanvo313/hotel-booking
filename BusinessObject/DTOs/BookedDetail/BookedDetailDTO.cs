﻿namespace BusinessObject.DTOs
{
    public class BookedDetailDTO
    {
        public BookedDetailDTO()
        {
         
        }
        
        public DateTime CheckInDate { get; set; }

        public DateTime CheckOutDate { get; set; }

        public decimal Amount { get; set; }

        public string RoomId { get; set; } = null!;

        public string BookingId { get; set; } = null!;

    }
}
