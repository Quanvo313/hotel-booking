﻿using Microsoft.EntityFrameworkCore;
using BusinessObject.Model;
namespace DataAccess;

public partial class HotelBookingContext : DbContext
{
    public HotelBookingContext()
    {
    }

    public HotelBookingContext(DbContextOptions<HotelBookingContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Account> Accounts { get; set; }

    public virtual DbSet<BookedDetail> BookedDetails { get; set; }

    public virtual DbSet<Booking> Bookings { get; set; }

    public virtual DbSet<Hotel> Hotels { get; set; }

    public virtual DbSet<Payment> Payments { get; set; }

    public virtual DbSet<Review> Reviews { get; set; }

    public virtual DbSet<Room> Rooms { get; set; }

    public virtual DbSet<RoomType> RoomTypes { get; set; }

    public virtual DbSet<User> Users { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Account>(entity =>
        {
            entity.HasKey(e => e.AccountId).HasName("PK__Accounts__349DA586F3D910C6");

            entity.Property(e => e.AccountId)
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasColumnName("AccountID");
            entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");
            entity.Property(e => e.Password)
                .HasMaxLength(70)
                .IsUnicode(false);
            entity.Property(e => e.Username)
                .HasMaxLength(20)
                .IsUnicode(false);
        });

        modelBuilder.Entity<BookedDetail>(entity =>
        {
            entity.HasKey(e => new { e.RoomId, e.BookingId }).HasName("PK__Booked_D__F5BF68B5E9875054");

            entity.ToTable("Booked_Detail");

            entity.Property(e => e.RoomId)
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasColumnName("RoomID");
            entity.Property(e => e.BookingId)
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasColumnName("BookingID");
            entity.Property(e => e.Amount).HasColumnType("money");
            entity.Property(e => e.CheckInDate).HasColumnType("datetime");
            entity.Property(e => e.CheckOutDate).HasColumnType("datetime");
            entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

            entity.HasOne(d => d.Booking).WithMany(p => p.BookedDetails)
                .HasForeignKey(d => d.BookingId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Booked_De__Booki__5DCAEF64");

            entity.HasOne(d => d.Room).WithMany(p => p.BookedDetails)
                .HasForeignKey(d => d.RoomId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Booked_De__RoomI__5CD6CB2B");
        });

        modelBuilder.Entity<Booking>(entity =>
        {
            entity.HasKey(e => e.BookingId).HasName("PK__Bookings__73951ACDF33C1FC8");

            entity.Property(e => e.BookingId)
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasColumnName("BookingID");
            entity.Property(e => e.BookingStatus)
                .HasMaxLength(10)
                .IsUnicode(false);
            entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");
            entity.Property(e => e.PaymentId)
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasColumnName("PaymentID");
            entity.Property(e => e.TotalAmount).HasColumnType("money");
            entity.Property(e => e.UserId)
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasColumnName("UserID");

            entity.HasOne(d => d.Payment).WithMany(p => p.Bookings)
                .HasForeignKey(d => d.PaymentId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Bookings__Paymen__571DF1D5");

            entity.HasOne(d => d.User).WithMany(p => p.Bookings)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Bookings__UserID__5812160E");
        });

        modelBuilder.Entity<Hotel>(entity =>
        {
            entity.HasKey(e => e.HotelId).HasName("PK__Hotel__46023BBFCB6C89D9");

            entity.ToTable("Hotel");

            entity.Property(e => e.HotelId)
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasColumnName("HotelID");
            entity.Property(e => e.Address)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.AverageRating).HasDefaultValueSql("((5))");
            entity.Property(e => e.Decription)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");
            entity.Property(e => e.Name)
                .HasMaxLength(100)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Payment>(entity =>
        {
            entity.HasKey(e => e.PaymentId).HasName("PK__Payments__9B556A5854560BD8");

            entity.Property(e => e.PaymentId)
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasColumnName("PaymentID");
            entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");
            entity.Property(e => e.PaymentDate).HasColumnType("datetime");
            entity.Property(e => e.PaymentMethod)
                .HasMaxLength(20)
                .IsUnicode(false);
            entity.Property(e => e.TotalPaymentAmount).HasColumnType("money");
        });

        modelBuilder.Entity<Review>(entity =>
        {
            entity.HasKey(e => e.ReviewId).HasName("PK__Reviews__74BC79AE8B9A8CBF");

            entity.Property(e => e.ReviewId)
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasColumnName("ReviewID");
            entity.Property(e => e.Comment)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.HotelId)
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasColumnName("HotelID");
            entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");
            entity.Property(e => e.UserId)
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasColumnName("UserID");

            entity.HasOne(d => d.Hotel).WithMany(p => p.Reviews)
                .HasForeignKey(d => d.HotelId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Reviews__HotelID__4E88ABD4");

            entity.HasOne(d => d.User).WithMany(p => p.Reviews)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Reviews__UserID__4F7CD00D");
        });

        modelBuilder.Entity<Room>(entity =>
        {
            entity.HasKey(e => e.RoomId).HasName("PK__Rooms__3286391942600171");

            entity.Property(e => e.RoomId)
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasColumnName("RoomID");
            entity.Property(e => e.HotelId)
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasColumnName("HotelID");
            entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");
            entity.Property(e => e.Name)
                .HasMaxLength(6)
                .IsUnicode(false);
            entity.Property(e => e.RoomTypeId)
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasColumnName("RoomTypeID");
            entity.Property(e => e.Status)
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.HasOne(d => d.Hotel).WithMany(p => p.Rooms)
                .HasForeignKey(d => d.HotelId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Rooms__HotelID__49C3F6B7");

            entity.HasOne(d => d.RoomType).WithMany(p => p.Rooms)
                .HasForeignKey(d => d.RoomTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Rooms__RoomTypeI__48CFD27E");
        });

        modelBuilder.Entity<RoomType>(entity =>
        {
            entity.HasKey(e => e.RoomTypeId).HasName("PK__RoomType__BCC89611E806BA1A");

            entity.Property(e => e.RoomTypeId)
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasColumnName("RoomTypeID");
            entity.Property(e => e.BasePrice).HasColumnType("money");
            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.Image).HasColumnType("text");
            entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");
            entity.Property(e => e.RoomTypeName)
                .HasMaxLength(6)
                .IsUnicode(false);
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.HasKey(e => e.UserId).HasName("PK__Users__1788CCACF8334162");

            entity.HasIndex(e => e.Email, "UQ__Users__A9D10534CB70C0FA").IsUnique();

            entity.Property(e => e.UserId)
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasColumnName("UserID");
            entity.Property(e => e.AccountId)
                .HasMaxLength(6)
                .IsUnicode(false)
                .HasColumnName("AccountID");
            entity.Property(e => e.Email)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");
            entity.Property(e => e.Name)
                .HasMaxLength(200)
                .IsUnicode(false);
            entity.Property(e => e.PhoneNumber)
                .HasMaxLength(10)
                .IsUnicode(false);

            entity.HasOne(d => d.Account).WithMany(p => p.Users)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("FK__Users__AccountID__3C69FB99");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
