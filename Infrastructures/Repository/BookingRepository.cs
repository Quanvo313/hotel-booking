﻿using Application.IRepository;
using AutoMapper;
using BusinessObject.DTOs.Booking;
using BusinessObject.Model;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repository
{
    public class BookingRepository : IBookingRepository
    {
        private readonly HotelBookingContext _context;
        private readonly IMapper _mapper;
        public BookingRepository(HotelBookingContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<BookingCreateDTO> CreateBooking(BookingCreateDTO booking)
        {
            await _context.Bookings.AddAsync(_mapper.Map<Booking>(booking));
            await _context.SaveChangesAsync();
            return booking;
        }

        public async Task<bool> DeleteBooking(string id)
        {
            var bookingcheck = await _context.Bookings.SingleOrDefaultAsync(booking => booking.BookingId.Equals(id));
            bookingcheck.IsDelete = true;
            var isSuccess = await _context.SaveChangesAsync() >= 1;
            return isSuccess;
        }

        public async Task<List<BookingDTO>> GetAll()
        {
            return _mapper.Map<List<BookingDTO>>(await _context.Bookings.ToListAsync());
        }

        public async Task<BookingDTO> GetBookingById(string id)
        {
            var booking = await _context.Bookings.SingleOrDefaultAsync(booking => booking.BookingId.Equals(id));
            return _mapper.Map<BookingDTO>(booking);
        }

        public async Task<BookingDTO> GetBookingByUserId(string id)
        {
            var booking = await _context.Bookings.SingleOrDefaultAsync(booking => booking.UserId.Equals(id));
            return _mapper.Map<BookingDTO>(booking);
        }

        public async Task<BookingDTO> UpdateBooking(BookingDTO booking)
        {
            var boookingUpdate = _mapper.Map<Booking>(booking);
            _context.Bookings.Update(boookingUpdate);
            await _context.SaveChangesAsync();
            return _mapper.Map<BookingDTO>(booking);
        }
    }
}
