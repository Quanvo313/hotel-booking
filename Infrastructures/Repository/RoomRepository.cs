﻿using Application.IRepository;
using BusinessObject.DTOs.Room;
using BusinessObject.Model;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repository
{
    public class RoomRepository : IRoomRepository
    {
        private readonly HotelBookingContext _context;

        public RoomRepository(HotelBookingContext context)
        {
            _context = context;
        }

        public async Task AddRoomAsync(Room room)
        {
            var existingRoom = await GetRoomByIdAsync(room.RoomId);
            if (existingRoom == null)
            {
                await _context.Rooms.AddAsync(room);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new Exception("The Room is already exist.");
            }
        }

        public async Task DeleteRoomAsync(string roomId)
        {
            var existingRoom = await GetRoomByIdAsync(roomId);
            if (existingRoom != null)
            {
                existingRoom.IsDelete = true;
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new Exception("The Room does not exist.");
            }
        }

        public async Task<IEnumerable<Room>> GetAllRoomsAsync()
        {
            return await _context.Rooms.ToListAsync();
        }

        public async Task<Room> GetRoomByIdAsync(string id)
        {
            return await _context.Rooms.SingleOrDefaultAsync(room => room.RoomId.Equals(id));
        }

        public async Task UpdateRoomAsync(Room room)
        {
            var existingRoom = await GetRoomByIdAsync(room.RoomId);
            if (existingRoom != null)
            {
                _context.Entry(existingRoom).CurrentValues.SetValues(room);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new Exception("The Room does not exist.");
            }
        }

        public async Task<List<RoomDTO>> SearchRoomByStatus(string status)
        {
            var st = await _context.Rooms.Where(r => r.Status.Equals(status))
                .Select(ob => new RoomDTO
                {
                    HotelID = ob.HotelId,
                    Name = ob.Name,
                    RoomID = ob.RoomId,
                    RoomTypeID = ob.RoomTypeId,
                    Status = ob.Status
                }).ToListAsync();

            return st;
        }
    }

}
