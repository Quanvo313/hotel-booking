﻿using Application.IRepository;
using BusinessObject.Model;
using BusinessObject.DTOs;
using DataAccess;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repository
{

    public class PaymentRepository : IPaymentRepository
    {
        private readonly HotelBookingContext _context;
        private readonly IMapper _mapper;

        public PaymentRepository(HotelBookingContext context,IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<PaymentCreateDTO> Insert(PaymentCreateDTO payment)
        {
            await _context.Payments.AddAsync(_mapper.Map<Payment>(payment));
            await _context.SaveChangesAsync();
            return payment;
        }

        public async Task<bool> Delete(string id)
        {
            Payment payment = null;
            payment = await _context.Payments.FindAsync(id);

            if (payment == null)
            {
                throw new Exception("Payment doesn't exist");
            }
            payment.IsDelete = true;
           await _context.SaveChangesAsync();
            return true;
        }

        public async Task<List<PaymentDTO>> GetAll()
        {
            var payments = await _context.Payments.Where(x => x.IsDelete == false).ToListAsync();
            return _mapper.Map<List<PaymentDTO>>(payments);
        }

        public async Task<IEnumerable<PaymentDTO>> GetByDate(DateTime date)
        {
            return await _context.Payments
                .Where(payment => payment.PaymentDate.Date == date.Date && payment.IsDelete != true)
                .Select(payment => new PaymentDTO
                {
                    PaymentDate = payment.PaymentDate,
                    PaymentId = payment.PaymentId,
                    PaymentMethod = payment.PaymentMethod,
                }).ToListAsync();
        }

        public async Task<IEnumerable<PaymentDTO>> GetByDateRange(DateTime from, DateTime to)
        {
            return await _context.Payments
                .Where(payment => payment.PaymentDate.Date >= from && payment.PaymentDate.Date <= to && payment.IsDelete != true)
                .Select(payment => new PaymentDTO
                {
                    PaymentDate = payment.PaymentDate,
                    PaymentId = payment.PaymentId,
                    PaymentMethod = payment.PaymentMethod,
                }).ToListAsync();
        }

        public async Task<PaymentDetailDTO> GetByID(string id)
        {
            return await _context.Payments.
                Where(payment => payment.PaymentId == id && payment.IsDelete != true)
                .Select(payment => new PaymentDetailDTO
                {
                    PaymentDate = payment.PaymentDate,
                    PaymentId = payment.PaymentId,
                    PaymentMethod = payment.PaymentMethod,
                    TotalPaymentAmount = payment.TotalPaymentAmount
                }).FirstOrDefaultAsync();
        }
        public async Task<PaymentDetailDTO> GetDetailByID(string id)
        {
            return await _context.Payments.
                Where(payment => payment.PaymentId == id && payment.IsDelete != true)
                .Select(payment => new PaymentDetailDTO
                {
                    PaymentDate = payment.PaymentDate,
                    PaymentId = payment.PaymentId,
                    PaymentMethod = payment.PaymentMethod,
                    TotalPaymentAmount = payment.TotalPaymentAmount
                }).FirstOrDefaultAsync();
        }

        public async Task<bool> Update(PaymentDetailDTO updatedPayment)
        {
            Payment existingPayment = _context.Payments
                 .Where(payment => payment.PaymentId == updatedPayment.PaymentId && payment.IsDelete != true)
                 .FirstOrDefault();

            if (existingPayment == null)
            {
                throw new InvalidOperationException("Payment not found.");
            }
            existingPayment.PaymentDate = updatedPayment.PaymentDate;
            existingPayment.TotalPaymentAmount = updatedPayment.TotalPaymentAmount;
            existingPayment.PaymentMethod = updatedPayment.PaymentMethod;
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
