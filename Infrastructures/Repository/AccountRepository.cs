﻿using Application.IRepository;
using BusinessObject.DTOs.Account;
using DataAccess;
using AutoMapper;
using BusinessObject.Model;
using Microsoft.EntityFrameworkCore;
using Application.Utils;

namespace Infrastructures.Repository
{
    public class AccountRepository : IAccountRepository
    {
        private readonly IMapper _mapper;
        private readonly HotelBookingContext _context;
        private readonly PasswordHashing _passwordHashing;
        public AccountRepository(HotelBookingContext context, IMapper mapper, PasswordHashing passwordHashing)
        {
            _context = context;
            _mapper = mapper;
            _passwordHashing = passwordHashing;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public async Task<AccountCreateDTO> AddAccount(AccountCreateDTO account)
        {
            await _context.Accounts.AddAsync(_mapper.Map<Account>(account));
            await _context.SaveChangesAsync();
            return account;
        }

        /// <summary>
        /// Soft delete account
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns>bool</returns>
        public async Task<bool> DeleteAccount(string accountId)
        {   
            Account account = await _context.Accounts.SingleOrDefaultAsync(obj => obj.AccountId.Equals(accountId));
            if(account != null) 
            {
                account.IsDelete = true;
                await _context.SaveChangesAsync();
                return true;
            }
            
            return false;
        }

        public async Task<AccountViewDTO> GetAccountById(string id)
        {
            Account account = await _context.Accounts.SingleOrDefaultAsync(obj => obj.AccountId.Equals(id));
            return _mapper.Map<AccountViewDTO>(account);
        }

        public async Task<List<AccountViewDTO>> GetAccounts()
        {
            var accounts = await _context.Accounts.ToListAsync();
            return _mapper.Map<List<AccountViewDTO>>(accounts);
        }

        public async Task<AccountViewDTO> Login(LoginDTO login)
        {
            var acc = await _context.Accounts.SingleOrDefaultAsync(obj => obj.Username.Equals(login.Username));
            if (acc != null)
            {
                if (_passwordHashing.VerifyPassword(login.Password, acc.Password) == true)
                {
                    return _mapper.Map<AccountViewDTO>(acc);
                }
            }
            return null;

        }

        public async Task<AccountViewDTO> UpdateAccount(AccountUpdateDTO accountUpdate)
        {
            var account = _mapper.Map<Account>(accountUpdate);
            _context.Accounts.Update(account);
            await _context.SaveChangesAsync();
            return _mapper.Map<AccountViewDTO>(account);
        }
    }
}
