﻿using Application.IRepository;
using BusinessObject.DTOs;
using DataAccess;
using BusinessObject.Model;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly HotelBookingContext _context;
        public UserRepository(HotelBookingContext context)
        {
            _context = context;
        }
        public async Task<List<UserDTO>> GetListUser()
        {
            var user = await _context.Users.Select(a => new UserDTO
            {
                UserId = a.UserId,
                Name = a.Name,
                Email = a.Email,
                PhoneNumber = a.PhoneNumber,
                IsDelete = a.IsDelete,
            }).ToListAsync();
            return user;
        }
        public async Task<UserDTO> GetUserById(string id)
        {
            var userid = await _context.Users.Where(a => a.UserId == id).SingleOrDefaultAsync();

            if (userid != null)
            {
                var useDTO = new UserDTO
                {
                    UserId = userid.UserId,
                    Name = userid.Name,
                    Email = userid.Email,
                    PhoneNumber = userid.PhoneNumber,
                    IsDelete = userid.IsDelete,
                };
                return useDTO;

            }
            return null;
        }

        public async Task<bool> DeleteUser(string id)
        {
            UserDTO _userDTO = await GetUserById(id);
            if (_userDTO != null)
            {
                if (_userDTO.IsDelete == false)
                {
                    _userDTO.IsDelete = true;
                    User _user = new User
                    {
                        UserId = _userDTO.UserId,
                        Name = _userDTO.Name,
                        Email = _userDTO.Email,
                        PhoneNumber = _userDTO.PhoneNumber,
                        IsDelete = _userDTO.IsDelete.Value
                    };
                    _context.Users.Update(_user);
                    await _context.SaveChangesAsync();
                    return true;
                }
            }
            else
            {
                throw new Exception("User does not valid");
            }

            return false;
        }
        public async Task<bool> UnDeleteUser(string id)
        {
            UserDTO _userDTO = await GetUserById(id);
            if (_userDTO != null)
            {
                if (_userDTO.IsDelete == true)
                {
                    _userDTO.IsDelete = false;
                    User _user = new User
                    {
                        //UserId = _userDTO.UserId,
                        //Name = _userDTO.Name,
                        //Email = _userDTO.Email,
                        //PhoneNumber = _userDTO.PhoneNumber,
                        IsDelete = _userDTO.IsDelete
                    };
                    _context.Users.Update(_user);
                }
            }
            else
            {
                throw new Exception("User does not exist");
            }
            await _context.SaveChangesAsync();
            return false;
        }

        public async Task<UserDTO> CreateUser(UserDTO userDTO)
        {
            if (userDTO != null)
            {
                string newId = await GenetareId();
                User newUser = new User
                {
                    UserId = newId,
                    Name = userDTO.Name,
                    Email = userDTO.Email,
                    PhoneNumber = userDTO.PhoneNumber,
                    IsDelete = false,
                };
                _context.Users.Add(newUser);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new Exception("User is invalid");
            }
            return userDTO;
        }

        public async Task<UserDTO> UpdateUser(UserDTO userDTO)
        {
            if (userDTO != null)
            {

                User userExist = await _context.Users.FindAsync(userDTO.UserId);
                if (userExist != null)
                {
                    userExist.Name = userDTO.Name;
                    userExist.Email = userDTO.Email;
                    userExist.PhoneNumber = userDTO.PhoneNumber;
                    _context.Users.Update(userExist);
                    await _context.SaveChangesAsync();
                }
                else { throw new Exception("User does not exist"); }
            }
            else
            {
                throw new Exception("User is invalid");
            }
            return userDTO;
        }

        public async Task<string> GenetareId()
        {
            var maxUserId = await _context.Users.MaxAsync(a => a.UserId);
            if (maxUserId == null)
            {
                return "U00001";
            }
            var LastId = int.Parse(maxUserId.Substring(1));
            var newId = LastId + 1;
            return $"U{newId.ToString("D5")}";
        }

        public async Task<IEnumerable<UserDTO>> GetUsersByName(string nameParam)
        {
            IEnumerable<UserDTO> users = null;
            users = await 
                _context.Users
                .Where(user => user.Name.ToLower().Contains(nameParam.ToLower()))
                .Select(user => new UserDTO
                {
                    UserId = user.UserId,
                    Name = user.Name,
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber,

                }).ToListAsync();
            return users;
        }

        public async Task<IEnumerable<UserDTO>> GetUsersByEmail(string emailParam)
        {
            IEnumerable<UserDTO> users = null;
            users = await
                _context.Users
                .Where(user => user.Email.ToLower().Contains(emailParam.ToLower()))
                .Select(user => new UserDTO
                {
                    UserId = user.UserId,
                    Name = user.Name,
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber,

                }).ToListAsync();
            return users;
        }

        public async Task<IEnumerable<UserDTO>> GetUsersByPhone(string phoneParam)
        {
            IEnumerable<UserDTO> users = null;
            users = await
                _context.Users
                .Where(user => user.PhoneNumber.ToLower().Contains(phoneParam.ToLower()))
                .Select(user => new UserDTO
                {
                    UserId = user.UserId,
                    Name = user.Name,
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber,

                }).ToListAsync();
            return users;
        }
    }
}
