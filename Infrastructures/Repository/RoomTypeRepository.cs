﻿using Application.IRepository;
using BusinessObject.DTOs;
using BusinessObject.Model;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repository
{
    public class RoomTypeRepository : IRoomTypeRepository
    {
        private readonly HotelBookingContext _context;

        public RoomTypeRepository(HotelBookingContext context)
        {
            _context = context;
        }
        public async Task addRoomType(RoomType roomType)
        {
            var rt = await _context.RoomTypes.AddAsync(roomType);
            _context.SaveChanges();
        }

        public async Task<IEnumerable<RoomType>> getAll()
        {
            List<RoomType> list = await _context.RoomTypes.ToListAsync();
            return list;
        }

        public async Task<RoomType> getRoomTypeId(string roomTypeId)
        {
            RoomType rt = await _context.RoomTypes.SingleOrDefaultAsync(item => item.RoomTypeId.Equals(roomTypeId));
            return rt;
        }

        public async Task<RoomType> removeRoomType(string roomTypeId)
        {

            RoomType Id = await getRoomTypeId(roomTypeId);
            Id.IsDelete = true;
            _context.RoomTypes.Update(Id);
            _context.SaveChanges();
            return Id;
        }

        public async Task<RoomType> updateRoomType(RoomType rt)
        {
            RoomType Id = await getRoomTypeId(rt.RoomTypeId);
            _context.Entry(Id).CurrentValues.SetValues(rt);
            _context.SaveChanges();
            return Id;
            //Nam chi doan nay
        }

        public async Task<RoomTypeDTO> GetRoomByRTName(string roomTypeName)
        {
            var RTname = await _context.RoomTypes.Where(a => a.RoomTypeName.Contains(roomTypeName))
                .Select(a => new RoomTypeDTO
                {
                    RoomTypeId = a.RoomTypeId,
                    RoomTypeName = a.RoomTypeName,
                    BasePrice = a.BasePrice,
                    Description = a.Description,
                    Image = a.Image
                }).FirstOrDefaultAsync();
            return RTname;
        }
    }
}
