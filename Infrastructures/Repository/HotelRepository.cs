// HotelRepository.cs
using Application.IRepository;
using AutoMapper;
using BusinessObject.DTOs.Hotel;
using BusinessObject.Model;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repository
{
    public class HotelRepository : IHotelRepository
    {
        private readonly IMapper _mapper;
        private readonly HotelBookingContext _context;

        public HotelRepository(HotelBookingContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<Hotel>> GetAllHotelsAsync()
        {
            return await _context.Hotels.ToListAsync();
        }

        public async Task<Hotel> GetHotelByIdAsync(string id)
        {
            return await _context.Hotels.SingleOrDefaultAsync(hotel => hotel.HotelId.Equals(id));
        }

        public async Task AddHotelAsync(HotelCreateDTO hotel)
        {
            _context.Hotels.Add(_mapper.Map<Hotel>(hotel));
            await _context.SaveChangesAsync();

        }

        public async Task UpdateHotelAsync(Hotel hotel)
        {
            var existingHotel = await GetHotelByIdAsync(hotel.HotelId);
            if (existingHotel != null)
            {
                _context.Entry(existingHotel).CurrentValues.SetValues(hotel);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new Exception("The Hotel does not exist.");
            }
        }

        public async Task DeleteHotelAsync(string hotelId)
        {
            var existingHotel = await GetHotelByIdAsync(hotelId);
            if (existingHotel != null)
            {
                existingHotel.IsDelete = true;
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new Exception("The Hotel does not exist.");
            }
        }

        public async Task<List<HotelDTO>> SearchHotelByAddress(string searchAddress)
        {
            var address = await _context.Hotels.Where(a=>a.Address.Contains(searchAddress))
            .Select(obj => new HotelDTO
            {
                Name = obj.Name,
                Address = obj.Address,
                Image = obj.Image,
                AverageRating = obj.AverageRating,
                Decription = obj.Decription
            }).ToListAsync();

                return address;
        }

        public async Task<List<HotelDTO>> GetHotelByName(string name)
        {
            var hotels = await _context.Hotels.Where(hotel => hotel.Name.Contains(name)).ToListAsync();
            return _mapper.Map<List<HotelDTO>>(hotels);
        }
    }
}
