﻿using Application.IRepository;
using AutoMapper;
using BusinessObject.DTOs;
using BusinessObject.Model;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repository
{
    public class BookedDetailRepository : IBookedDetailRepository
    {
        private readonly IMapper _mapper;
        private readonly HotelBookingContext _context;
        public BookedDetailRepository(HotelBookingContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<BookedDetailDTO>> GetListBookedDetails()
        {
            var details = await _context.BookedDetails.ToListAsync();
            return _mapper.Map<List<BookedDetailDTO>>(details);
        }

        public async Task<BookedDetailDTO> GetBookedDetailsById(string id)
        {
            var detaisId = await _context.BookedDetails.Where(a => a.BookingId == id).SingleOrDefaultAsync();
            if (detaisId != null)
            {
                var detailDTO = _mapper.Map<BookedDetailDTO>(detaisId);
                return detailDTO;
            }
            return null;
        }

        public async Task<BookedDetailDTO> CreateBookedDetail(BookedDetailDTO bookedDetailDTO)
        {
            var room = await _context.Rooms.SingleOrDefaultAsync(r => r.RoomId.Equals(bookedDetailDTO.RoomId));
            var roomType = await _context.RoomTypes.SingleOrDefaultAsync(rt => rt.RoomTypeId.Equals(room.RoomTypeId));
            // Ánh xạ giá trị BasePrice từ RoomType vào Amount của BookedDetailCreateDTO
            bookedDetailDTO.Amount = roomType.BasePrice;
            // Các thuộc tính khác của BookedDetailCreateDTO
            bookedDetailDTO.BookingId = bookedDetailDTO.BookingId;
            var bookedDetail = _mapper.Map<BookedDetail>(bookedDetailDTO);
            await _context.BookedDetails.AddAsync(bookedDetail);
            await _context.SaveChangesAsync();
            return bookedDetailDTO;
        }

        public async Task<BookedDetailDTO> UpdateBookedDetail(BookedDetailDTO bookedDetailDTO)
        {
            var detail = _mapper.Map<BookedDetail>(bookedDetailDTO);
            _context.BookedDetails.Update(detail);
            await _context.SaveChangesAsync();
            return _mapper.Map<BookedDetailDTO>(detail);
        }

        public async Task<bool> DeleteBookedDetail(string id)
        {
            var detail = await _context.BookedDetails.Where(a => a.BookingId == id).SingleOrDefaultAsync();
            if (detail != null && detail.IsDelete == false)
            {
                detail.IsDelete = true;
                _context.BookedDetails.Update(detail);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> UnDeleteBookedDetail(string id)
        {
            var detail = await _context.BookedDetails.Where(a => a.BookingId == id).SingleOrDefaultAsync();
            if (detail != null && detail.IsDelete == true)
            {
                detail.IsDelete = false;
                _context.BookedDetails.Update(detail);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}
