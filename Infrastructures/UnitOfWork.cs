﻿using Application;
using Application.IRepository;
using DataAccess;

namespace Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly HotelBookingContext _context;
        public UnitOfWork(
            HotelBookingContext context,
            IAccountRepository accountRepository,
            IHotelRepository hotelRepository,
            IPaymentRepository paymentRepository,
            IRoomRepository roomRepository,
            IRoomTypeRepository roomTypeRepository,
            IUserRepository userRepository,
            IBookedDetailRepository bookedDetailRepository)
        {
            _context = context;
        }
        public IAccountRepository AccountRepository { get; }

        public IHotelRepository HotelRepository { get; }

        public IPaymentRepository PaymentRepository { get; }

        public IRoomTypeRepository RoomTypeRepository { get; }

        public IRoomRepository RoomRepository { get; }

        public IUserRepository UserRepository { get; }
        public IBookedDetailRepository BookedDetailRepository { get; }
    }
}
